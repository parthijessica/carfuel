package com.ttl.car_mileage.utilz;

import android.content.Context;
import android.content.SharedPreferences;

import com.facebook.BuildConfig;

public class LocalSession {

    public static final String USER_ID = "USER_ID";
    public static final String EMAIL = "EMAIL";
    public static final String NAME = "NAME";
    public static final String PHONE = "SHOP_LIST";
    private static String app_name = BuildConfig.APPLICATION_ID;
    public   static String isLogged ="false";




    public static void setBoolean(Context c, String key, boolean value) {
        if (c != null) {
            SharedPreferences.Editor editor = c.getSharedPreferences(app_name,
                    Context.MODE_PRIVATE).edit();
            editor.putBoolean(key, value);
            editor.commit();
        }
    }
    public static Boolean getBoolean(Context c, String key,Boolean default_value) {
        if (c == null) {
            return default_value;
        } else {
            SharedPreferences prefs = c.getSharedPreferences(app_name,
                    Context.MODE_PRIVATE);
            return prefs.getBoolean(key, default_value);
        }
    }
    public static void setString(Context c, String key, String value) {
        if (c != null) {
            SharedPreferences.Editor editor = c.getSharedPreferences(app_name,
                    Context.MODE_PRIVATE).edit();
            editor.putString(key, value);
            editor.commit();
        }
    }
    public static String getString(Context c, String key,String default_value) {
        if (c == null) {
            return default_value;
        } else {
            SharedPreferences prefs = c.getSharedPreferences(app_name,
                    Context.MODE_PRIVATE);
            return prefs.getString(key, default_value);
        }
    }


}
