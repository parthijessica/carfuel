package com.ttl.car_mileage.utilz;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.provider.MediaStore;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;


import com.ttl.car_mileage.R;

import java.util.ArrayList;

@SuppressLint("StaticFieldLeak")
public class CameraGallery {

    private static ImageView imgCancelProducts;
    private static TextView txtCameraAddPrt;
    private static TextView txtGalleryAddPrt;
    public static int REQUEST_IMAGE_CAPTURE = 7873;
    public static int SELECT_IMAGE = 6565;

    public static void showSelectCameraGellery(FragmentActivity activity, boolean isMultiSelect) {
        try {
            Dialog optionCG = new Dialog(activity);
            optionCG.requestWindowFeature(Window.FEATURE_NO_TITLE);
            optionCG.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            optionCG.setContentView(R.layout.dialog_show_camera_gellary);
            optionCG.show();
            imgCancelProducts = optionCG.findViewById(R.id.img_cancel_products);
            txtCameraAddPrt = optionCG.findViewById(R.id.txt_camera_addPrt);
            txtGalleryAddPrt = optionCG.findViewById(R.id.txt_gallery_addPrt);
            ArrayList<String> permissionList = new ArrayList<>();

            txtCameraAddPrt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    permissionList.add(Manifest.permission.CAMERA);
                    if (PermissionUtilz.runtimePer(activity, permissionList, true)) {
                        optionCG.dismiss();
                        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        if (takePictureIntent.resolveActivity(activity.getPackageManager()) != null) {
                            activity.startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
                        }
                    }

                }
            });

            txtGalleryAddPrt.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    permissionList.add(Manifest.permission.READ_EXTERNAL_STORAGE);
                    permissionList.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
                    if (PermissionUtilz.runtimePer(activity, permissionList, true)) {
                        optionCG.dismiss();
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        if (isMultiSelect) {
                            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                        }
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        activity.startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_IMAGE);
                    }
                }
            });

            imgCancelProducts.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    optionCG.dismiss();
                }
            });


        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
