package com.ttl.car_mileage.utilz;

public interface Constants {
    //    String BASE_URL = "http://3.21.245.254/app/api/v1/";
    String BASE_URL = "http://3.21.245.254:8080/api/v1/";
    String SALT = "Biz@Text@!*><2020";

    int PDF_REQUESTT_CODE = 8888;

    /**
     * Chat documents  name for Folder like  pdf,image,
     */
    String PDF_FOLDER = "Chat_PDF";
    String CHAT_THUMB = "Chat_bitmp";
    String IMAGE_FOLDER = "Chat_Image";

    /**
     * TYpe Of  File  using  for  chatting  view design for Example
     * send text ,image  receive  text ,image
     * send pdf and  receive  Pdf
     */
    int SEND_TEXT = 1;
    int SEND_IMAGE = 2;
    int SEND_PDF = 3;
    int RECEIVE_PDF = 4;
    int RECEIVE_IMAGE = 5;
    int RECEIVE_TEXT = 6;

    String GET = "GET";
    String POST = "POST";

    String GETFORM = "GETFORM";
    String POSTFORM = "POSTFORM";
    String POSTBODY = "POSTBODY";
    String POSTPART = "POSTPART";
    String POSTPARTARRA = "POSTPARTARRA";

    String VERIFYBUSINESS = "VerifyBusiness";
    String ENTEROTP = "EnterOTP";
    String PRICE_HIGH_TO_LOW = "PRICE_HIGH_TO_LOW";
    String PRICE_LOW_TO_HIGH = "PRICE_LOW_TO_HIGH";


    /**
     * API  NAMES  LIKE
     */
    String SHOPS_ADD_API = "shops/add";
    String USERS_SHOP_OWNER_REGISTER_API = "users/shop-owner-register";
    String SHOPS_API = "shops";
    String SHOPS_SEND_OTP_API = "shops/send-otp";
    String SHOPS_VERIFY_OTP_API = "shops/verify-otp";
    String TICKET_TYPES_API = "ticket-types";
    String TTICKETS_API = "tickets";
    String TICKET_TYPES_ADD_API = "ticket-types/add";
    String TICKET_TYPES_DELETE_API = "ticket-types/delete";
    String TICKET_TYPES_UPDATE_API = "ticket-types/edit";
    String TICKETS_CLOSE_API = "tickets/close";
    String PRODUCT_CATEGORIES_API = "product-categories";
    String PRODUCT_CATEGORIES_ADD_API = "product-categories/add";
    String PRODUCTS_API = "products";
    String PRODUCT_CATEGORIES_EDIT_API = "product-categories/edit";
    String PRODUCT_CATEGORIES_DELETE_API = "product-categories/delete";
    String PRODUCTS_ADD_API = "products/add";
    String PRODUCTS_UPDATE_API = "products/update";
    String PRODUCTS_DELETE_API = "products/delete";
    String PRODUCTS_DELETE_PHOTO_API = "products/delete-photo";
    String TICKETS_REOPEN_API = "tickets/reopen";
    String USERS_SHOP_CUSTOMERS_API = "users/shop-customers";
    String USERS_BLOCK_UNBLOCK_CUSTOMER_API = "users/block-unblock-customer";
    String USERS_ADD_CUSTOMER_API = "users/add-customer";
    String USERS_ADD_EMPLOYEE_API = "users/add-employee";
    String USERS_EMPLOYEES_API = "users/employees";
    String USERS_DELETE_EMPLOYEE_API = "users/delete-employee";
    String USERS_UPDATE_EMPLOYEE_API = "users/update-employee";


}
