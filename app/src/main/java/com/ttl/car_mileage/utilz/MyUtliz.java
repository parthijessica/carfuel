package com.ttl.biztextshopper.utilz;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.ParseException;
import android.net.Uri;
import android.os.Build;
import android.os.CountDownTimer;
import android.os.Environment;
import android.os.Handler;
import android.os.ParcelFileDescriptor;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Base64OutputStream;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.firestore.GeoPoint;
import com.shockwave.pdfium.PdfDocument;
import com.shockwave.pdfium.PdfiumCore;
import com.squareup.picasso.Picasso;
import com.ttl.biztextshopper.BuildConfig;
import com.ttl.biztextshopper.R;


import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static com.ttl.biztextshopper.utilz.Constants.DATE_FORMAT;
import static com.ttl.biztextshopper.utilz.Constants.DAY_FORMAT;
import static com.ttl.biztextshopper.utilz.Constants.TIME_FORMAT;

public class MyUtliz {

    static String FDER = Environment.getExternalStorageDirectory() + "/Android/data/" + BuildConfig.APPLICATION_ID + "/";
    static Snackbar snackbar = null;
    public static final String DATEFORMAT_UTC = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";

    static Dialog dialog;
    private static int LOCATION_CURRENT = 7777;


    /**
     * check current device iits online
     */
    public static boolean isOnline(Context context) {
        try {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            //should check null because in airplane mode it will be null
            return (netInfo != null && netInfo.isConnected());
        } catch (NullPointerException e) {
            e.printStackTrace();
            return false;
        }
    }


    /**
     * Only Chatting Activity added fragment
     */
    public static void addFragmentChat(FragmentActivity activity, Fragment fragment) {
        try {
            FragmentManager manager = activity.getSupportFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.add(R.id.chat_frame_container, fragment).commit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void replaceFragmentChat(FragmentActivity activity, Fragment fragment) {
        try {
            FragmentManager manager = activity.getSupportFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.replace(R.id.chat_frame_container, fragment).commit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void replaceFragmentChat(FragmentActivity activity, Fragment fragment, int i) {
        try {
            FragmentManager manager = activity.getSupportFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.replace(i, fragment).commit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    /**
     * Open the Mobile Device Setting for enable to  mobile  networking setting
     */
    public static void openSetting(FragmentActivity activity) {
        try {
            activity.startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Custom SnackBar Functionality
     */
    @SuppressLint("NewApi")
    public static Snackbar showSnackBar(FragmentActivity context, View view, String message) {

        snackbar = null;
        try {
            snackbar = Snackbar.make(view, message, Snackbar.LENGTH_LONG);
            View snackBarView = snackbar.getView();
            snackBarView.setBackgroundColor(ContextCompat.getColor(context, R.color.orange_ticket_list));
            TextView textView = snackBarView.findViewById(com.google.android.material.R.id.snackbar_text);
            //textView.setTextAppearance(R.style.textviewSEMIBOlD);
            textView.setTextSize(16);
            textView.setTextColor(Color.WHITE);
            snackbar.show();
            return snackbar;
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
        return snackbar;
    }

    /**
     * Snackbar dismiss if Open
     *
     * @param snackBar
     */
    public static void dismissSnackBar(Snackbar snackBar) {
        try {
            if (snackBar != null) {
                if (snackBar.isShown()) {
                    snackBar.dismiss();
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Custom  top right Position to Dialog   show
     */
    public static void setPositionDialogTOPRGHT(int xValue, int yValue, Dialog dialog) {
        Window window = dialog.getWindow();
        WindowManager.LayoutParams param = window.getAttributes();
        param.gravity = Gravity.TOP | Gravity.RIGHT;
        param.y = yValue;
        param.x = xValue;
        window.setAttributes(param);
        window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        window.setBackgroundDrawableResource(android.R.color.transparent);
    }

    public static void setPositionBotmLEFT(int xValue, int yValue, Dialog dialog) {
        Window window = dialog.getWindow();
        WindowManager.LayoutParams param = window.getAttributes();
        param.gravity = Gravity.BOTTOM | Gravity.LEFT;
        param.y = yValue;
        param.x = xValue;
        window.setAttributes(param);
        window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        window.setBackgroundDrawableResource(android.R.color.transparent);
    }

    public static void setPositionBotmRIGHT(int xValue, int yValue, Dialog dialog) {
        Window window = dialog.getWindow();
        WindowManager.LayoutParams param = window.getAttributes();
        param.gravity = Gravity.BOTTOM | Gravity.RIGHT;
        param.y = yValue;
        param.x = xValue;
        window.setAttributes(param);
        window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        window.setBackgroundDrawableResource(android.R.color.transparent);
    }

    /**
     * Generating thumb  image
     */
    public static File generateImageFromPdf(Uri pdfUri, FragmentActivity activity) {
        File file = null;
        int pageNumber = 0;
        PdfiumCore pdfiumCore = new PdfiumCore(activity);
        try {
            ParcelFileDescriptor fd = activity.getContentResolver().openFileDescriptor(pdfUri, "r");
//            PdfDocument pdfDocument = pdfiumCore.newDocument(fd);
            PdfDocument pdfDocument = pdfiumCore.newDocument(fd);
            pdfiumCore.openPage(pdfDocument, pageNumber);
            int width = pdfiumCore.getPageWidthPoint(pdfDocument, pageNumber);
            int height = pdfiumCore.getPageHeightPoint(pdfDocument, pageNumber);
            Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
            pdfiumCore.renderPageBitmap(pdfDocument, bmp, pageNumber, 0, 0, width, height);
            file = saveImage(bmp, pdfUri, activity);
            pdfiumCore.closeDocument(pdfDocument); // important!

        } catch (Exception e) {
            e.printStackTrace();
            return file;
        }
        return file;
    }

    private static File saveImage(Bitmap bmp, Uri pdfUri, FragmentActivity activity) {
        FileOutputStream out = null;
        File file = null;
        try {

            file = fileFolderCreated("GanaratingIMG", PathUtilz.getPathFromUri(activity, pdfUri).getName());
            out = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.PNG, 100, out); // bmp is your Bitmap instance
        } catch (Exception e) {
            //todo with exception
        } finally {
            try {
                if (out != null)
                    out.close();
            } catch (Exception e) {
                //todo with exception
            }
        }
        return file;
    }

    /**
     * Load toImage  from file or Server
     */
    public static void loadSVRIMG(File file, ImageView imageView, boolean isCircle) {
        try {
            if (isCircle) {
                Picasso.get()
                        .load(file)
                        .transform(new CircleTransform())
                        .placeholder(R.drawable.logo_text)
                        .fit()
                        .error(R.drawable.logo_text)
                        .into(imageView);
            } else {
                Picasso.get()
                        .load(file)
                        .placeholder(R.drawable.logo_text)
                        .fit()
                        .error(R.drawable.logo_text)
                        .into(imageView);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void loadIMG_DRW(int i, ImageView imageView) {
        try {


            Picasso.get()
                    .load(i)
                    .placeholder(R.drawable.logo_text)
                    .fit()
                    .error(R.drawable.logo_text)
                    .into(imageView);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void loadSVRIMG(String path, ImageView imageView, boolean isCircle) {
        try {
            if (isCircle) {
                Picasso.get()
                        .load(path)
                        .transform(new CircleTransform())
                        .placeholder(R.drawable.logo_text)
                        .fit()
                        .error(R.drawable.logo_text)
                        .into(imageView);
            } else {
                Picasso.get()
                        .load(path)
                        .placeholder(R.drawable.logo_text)
                        .fit()
                        .error(R.drawable.logo_text)
                        .into(imageView);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void loadSVRIMGNOPLACEHOLDER(String path, ImageView imageView, boolean isCircle) {
        try {
            if (isCircle) {
                Picasso.get()
                        .load(path)
                        .transform(new CircleTransform())
                        .fit()
                        .into(imageView);
            } else {
                Picasso.get()
                        .load(path)
                        .fit()
                        .into(imageView);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void loadSVRCEN(String path, ImageView imageView) {
        try {
            Picasso.get()
                    .load(path)
                    .placeholder(R.drawable.logo_text)
                    .centerCrop()
                    .resize(200, 220)
                    .error(R.drawable.logo_text)
                    .into(imageView);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void loadCIRCLEIMAGE(int path, ImageView imageView) {
        try {
            Picasso.get()
                    .load(path)
                    .transform(new CircleTransform())
                    .placeholder(R.drawable.sample_test)
                    .fit()
                    .error(R.drawable.sample_test)
                    .into(imageView);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * isDirected there  or  creadeted
     */
    public static File fileFolderCreated(String folderName, String filename) {
        File file = null;
        try {
            File folder = new File(FDER + folderName);
            if (!folder.exists())
                folder.mkdirs();
            file = new File(folder, filename);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return file;
    }

    public static Dialog showLoading(FragmentActivity activity, boolean isCanceld) {
        dialog = new Dialog(activity);
        try {
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(isCanceld);
            dialog.setContentView(R.layout.dialog_item_loading);
            dialog.show();
            dialog.getWindow().setGravity(Gravity.CENTER);
            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            ImageView imageView = dialog.findViewById(R.id.dia_loading_img);
            rotationView(activity, imageView);

        } catch (Exception s) {
            s.printStackTrace();
        }
        return dialog;
    }

    public static void loadingDismiss() {
        try {
            if (dialog != null && dialog.isShowing()) {
                dialog.dismiss();
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void rotationView(FragmentActivity activity, View view) {
        Animation animationUtils = AnimationUtils.loadAnimation(activity, R.anim.rotation);
        view.startAnimation(animationUtils);
    }

    /**
     * Show internet Error text view
     */
    public static void checkNetHideView(Context getContext, TextView view, int DownOrUp) {
        try {

            Animation aniSlide = AnimationUtils.loadAnimation(getContext, DownOrUp);
            view.startAnimation(aniSlide);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                view.setBackgroundColor(getContext.getColor(R.color.green_500));
            } else {
                view.setBackgroundColor(getContext.getResources().getColor(R.color.green_500));
            }
            view.setText(getContext.getString(R.string.connecting));
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    view.setVisibility(View.GONE);
                }
            }, 3000);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void checkNetShowView(FragmentActivity getContext, TextView view, int DownOrUp) {
        try {
            view.setVisibility(View.VISIBLE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                view.setBackgroundColor(getContext.getColor(R.color.red));
            } else {
                view.setBackgroundColor(getContext.getResources().getColor(R.color.red));
            }
            view.setText(getContext.getString(R.string.please_check_your_internet_connection_goto));
            Animation aniSlide = AnimationUtils.loadAnimation(getContext, DownOrUp);
            view.startAnimation(aniSlide);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openSetting(getContext);
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    /**
     * gettig HASH KEY IN Coding
     */
    public static void getHASHKEY(FragmentActivity activity) {
        try {
            PackageInfo info = activity.getPackageManager().getPackageInfo(activity.getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }
    }


    /**
     * MD5 Encryption
     *
     * @param s
     * @return
     */
    public static String md5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = MessageDigest.getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static Bitmap getScreenShot(View view) {
        Bitmap image = Bitmap.createBitmap(view.getWidth(),
                view.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas b = new Canvas(image);
        view.draw(b);
        return image;

    }

    /**
     * merging Photos Funcitonality
     *
     * @param bmp1
     * @param bmp2
     * @return
     */
    public static Bitmap overlayTOPBOTTOM(Bitmap bmp1, Bitmap bmp2) {
        Bitmap bmOverlay = Bitmap.createBitmap(bmp1.getWidth(), bmp1.getHeight(), bmp1.getConfig());
        Canvas canvas = new Canvas(bmOverlay);
        canvas.drawBitmap(Bitmap.createScaledBitmap(bmp1, bmp1.getWidth(), (bmp1.getHeight() / 4) * 3, false), 0, 0, null);
        canvas.drawBitmap(Bitmap.createScaledBitmap(bmp2, bmp1.getWidth(), (bmp1.getHeight() / 4), false), 0, Bitmap.createScaledBitmap(bmp1, bmp1.getWidth(), (bmp1.getHeight() / 4) * 3, false).getHeight(), null);
        return bmOverlay;
    }

    public static Bitmap overlayLEFTRIGHT(Bitmap bmp1, Bitmap bmp2) {
        Bitmap bmOverlay = Bitmap.createBitmap(bmp1.getWidth(), bmp1.getHeight(), bmp1.getConfig());
        Canvas canvas = new Canvas(bmOverlay);
        canvas.drawBitmap(Bitmap.createScaledBitmap(bmp1, bmp1.getWidth() / 2, bmp1.getHeight(), false), 0, 0, null);
        canvas.drawBitmap(Bitmap.createScaledBitmap(bmp2, bmp1.getWidth() / 2, bmp1.getHeight(), false), Bitmap.createScaledBitmap(bmp1, bmp1.getWidth() / 2, bmp1.getHeight(), false).getWidth(), 0, null);
        return bmOverlay;
    }


    /**
     * Sharing Fuantionality
     */
    public static void shareviaintent(FragmentActivity activity, File file) {
        try {
            Uri bmpUri;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                bmpUri = FileProvider.getUriForFile(activity, activity.getApplicationContext().getPackageName() + ".fileprovider", file);
            } else {
                bmpUri = Uri.fromFile(file);
            }
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.putExtra(Intent.EXTRA_STREAM, bmpUri);
            intent.setType("image/*");
            activity.startActivity(Intent.createChooser(intent, file.getName()));

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    public static File conBitmapToFile(Bitmap bitmap, File file) {
        try {

            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, 0 /*ignored for PNG*/, bos);
            byte[] bitmapdata = bos.toByteArray();
            //write the bytes in file
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(bitmapdata);
            fos.flush();
            fos.close();
        } catch (Exception eex) {
            eex.printStackTrace();
        }
        return file;
    }

    public static Bitmap conURItoBitmap(Uri uri, FragmentActivity activity) {
        Bitmap bitmap = null;
        try {
            bitmap = BitmapFactory.decodeFile(PathUtilz.getPathFromUri(activity, uri).getAbsolutePath());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return bitmap;
    }

    /**
     * cresting Multipart using  name and file
     */
    public static MultipartBody.Part creatingPart(String name, File file) {
        MultipartBody.Part body = null;
        try {
            RequestBody requestFile = RequestBody.create(file, MediaType.parse("image/*"));
            body = MultipartBody.Part.createFormData(name, file.getName(), requestFile);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return body;
    }

    /**
     * Checking Valid Foramt  email address
     *
     * @param target
     * @return
     */
    public static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static RequestBody createRequestBody(String var) {
        return RequestBody.create(var, MediaType.parse("multipart/form-data"));
    }


    /**
     * JSON GET  STRING URL
     *
     * @param
     * @return
     */
    public static Long getTimemillisecondUTC(String value) {

        SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.000'Z'");
        input.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date d = null;
        try {
            d = input.parse(value);
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (java.text.ParseException e) {
            e.printStackTrace();

        }
        return d.getTime();
    }

    public static Long getTimeMillisecond(String value) {
        SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date d = null;
        try {
            d = input.parse(value);
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (java.text.ParseException e) {
            e.printStackTrace();

        }
        return d.getTime();
    }

    public static String getDate(long milliSeconds, String dateFormat) {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    public static String getDateUTC(long milliSeconds, String dateFormat) {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        formatter.setCalendar(calendar);
        formatter.setTimeZone(TimeZone.getTimeZone("Asia/Kolkata"));
        return formatter.format(calendar.getTime());
    }


    public static String[] convertStrListTostrArr(ArrayList<String> mStringList) {
        String[] mStringArray = new String[mStringList.size()];
        mStringArray = mStringList.toArray(mStringArray);

        for (int i = 0; i < mStringArray.length; i++) {
            Log.d("string is", (String) mStringArray[i]);
        }
        return mStringArray;
    }


    /**
     * gettuing  locaiton  name and  city  state  pincode
     */
    public static Address getAddress(Context activity, double lat, double lng) {
        Address obj = null;
        Geocoder geocoder = new Geocoder(activity, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);
            obj = addresses.get(0);
            Log.v("IGA", "Address" + obj.getAddressLine(0));
            // Toast.makeText(this, "Address=>" + add,
            // Toast.LENGTH_SHORT).show();

            // TennisAppActivity.showDialog(add);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Toast.makeText(activity, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return obj;
    }

    /**
     * getting  lat long  from  address
     */
    public static Address getLocationFromAddress(String strAddress, FragmentActivity activity) {

        Geocoder coder = new Geocoder(activity);
        List<Address> address;
        Address location = null;

        try {
            address = coder.getFromLocationName(strAddress, 5);
            if (address == null) {
                return null;
            }
            location = address.get(0);
            location.getLatitude();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return location;
    }


    /**
     * Open the Mobile Device Setting for enable to  mobile  networking Location
     */
    public static void openLocaiton(FragmentActivity activity) {
        try {
            activity.startActivityForResult(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS), LOCATION_CURRENT);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static Boolean isLocationEnabled(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            // This is new method provided in API 28
            LocationManager lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
            return lm.isLocationEnabled();
        } else {
            // This is Deprecated in API 28
            int mode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE,
                    Settings.Secure.LOCATION_MODE_OFF);
            return (mode != Settings.Secure.LOCATION_MODE_OFF);
        }
    }

    public static String getBase64Encode(File f) {
        InputStream inputStream = null;
        String encodedFile = "", lastVal;
        try {
            inputStream = new FileInputStream(f.getAbsolutePath());
            byte[] buffer = new byte[10240];//specify the size to allow
            int bytesRead;
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            Base64OutputStream output64 = new Base64OutputStream(output, Base64.DEFAULT);

            while ((bytesRead = inputStream.read(buffer)) != -1) {
                output64.write(buffer, 0, bytesRead);
            }
            output64.close();
            encodedFile = output.toString();
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        lastVal = encodedFile;
        return lastVal;
    }

    public static File downloadedFile(String fileServerPath, File fileName) {
        File file = null;
        int count;
        try {
            URL url = new URL(fileServerPath);
            URLConnection conection = url.openConnection();
            conection.connect();

            // this will be useful so that you can show a tipical 0-100%
            // progress bar
            int lenghtOfFile = conection.getContentLength();

            // download the file
            InputStream input = new BufferedInputStream(url.openStream(),
                    8192);

            // Output stream
            OutputStream output = new FileOutputStream(fileName);

            byte data[] = new byte[1024];

            long total = 0;

            while ((count = input.read(data)) != -1) {
                total += count;
                // publishing the progress....
                // After this onProgressUpdate will be called

                // writing data to file
                output.write(data, 0, count);
            }

            // flushing output
            output.flush();

            // closing streams
            output.close();
            input.close();
            file = fileName;
        } catch (Exception e) {
        }
        return file;
    }

    private static File downloadFile(String url, File outputFile) {
        File file = null;
        try {
            URL u = new URL(url);
            URLConnection conn = u.openConnection();
            int contentLength = conn.getContentLength();

            DataInputStream stream = new DataInputStream(u.openStream());

            byte[] buffer = new byte[contentLength];
            stream.readFully(buffer);
            stream.close();

            DataOutputStream fos = new DataOutputStream(new FileOutputStream(outputFile));
            fos.write(buffer);
            fos.flush();
            fos.close();
            file = outputFile;
        } catch (FileNotFoundException e) {

        } catch (IOException e) {

        }
        return file;
    }

    public static void openGoogleMaps(FragmentActivity activity, String latitude, String longitude, String address) {
        try {
//            String uri = String.format(Locale.ENGLISH, "geo:%f,%f", latitude, longitude);
//            String geoUri = "http://maps.google.com/maps?q=loc:" + latitude + "," + longitude + " (" + address + ")";
            Uri uri = Uri.parse("geo:" + latitude + "," + longitude + "?q=" + address);
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            activity.startActivity(intent);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @SuppressLint("MissingPermission")
    public static void openCall(FragmentActivity activity, String mobileNumber) {
        try {
            ArrayList<String> strings =new ArrayList<>();
            strings.add(Manifest.permission.CALL_PHONE);
            if (PermissionUtilz.hasPermissions(activity,strings)) {
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + mobileNumber));
                activity.startActivity(intent);
            }else {
                PermissionUtilz.runtimePer(activity,strings,true);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static String isVAlidateStr(String firstName) {
        if (firstName != null) {
            return firstName.trim();
        } else {
            return "";
        }
    }

    public static void showToastNormal(FragmentActivity activity, String s) {
        try {
            Toast.makeText(activity, s, Toast.LENGTH_LONG).show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void showToast(FragmentActivity activity, String s) {
        try {
            LayoutInflater inflater = activity.getLayoutInflater();
            View layout = inflater.inflate(R.layout.toast_layout, null, false);
            TextView text = (TextView) layout.findViewById(R.id.txtToastMessage);
            text.setText(s);
            Toast toast = new Toast(activity.getApplicationContext());
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toast.setDuration(0);
            toast.setView(layout);
            CountDownTimer countDownTimer = new CountDownTimer(500, 300) {
                @Override
                public void onTick(long l) {
                    toast.show();
                }

                @Override
                public void onFinish() {
                    toast.cancel();
                }
            };
            toast.show();
            countDownTimer.start();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void showToastLONG(FragmentActivity activity, String s) {
        try {
            LayoutInflater inflater = activity.getLayoutInflater();
            View layout = inflater.inflate(R.layout.toast_layout, null, false);
            TextView text = (TextView) layout.findViewById(R.id.txtToastMessage);
            text.setText(s);
            Toast toast = new Toast(activity.getApplicationContext());
            toast.setGravity(Gravity.CENTER_VERTICAL, 0, 0);
            toast.setDuration(0);
            toast.setView(layout);
            CountDownTimer countDownTimer = new CountDownTimer(5000, 300) {
                @Override
                public void onTick(long l) {
                    toast.show();
                }

                @Override
                public void onFinish() {
                    toast.cancel();
                }
            };
            toast.show();
            countDownTimer.start();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static String checkDateIsToday(long timeMilisecond) {
        Calendar currentCalender = Calendar.getInstance();
        Calendar receivedCalender = Calendar.getInstance();
        receivedCalender.setTimeInMillis(timeMilisecond);
        int cuDay = currentCalender.get(Calendar.DAY_OF_MONTH);
        int cuMonth = currentCalender.get(Calendar.MONTH);
        int cuYear = currentCalender.get(Calendar.YEAR);
        int recDay = receivedCalender.get(Calendar.DAY_OF_MONTH);
        int recMonth = currentCalender.get(Calendar.MONTH);
        int recYear = currentCalender.get(Calendar.YEAR);
        int diffDay = cuDay - recDay;
        Log.d("checkDateIsToday", ": " + cuDay + recDay + diffDay);
        if (cuYear == recYear) {
            if (cuMonth == recMonth) {
                if (cuDay == recDay) {
                    return getDate(receivedCalender.getTimeInMillis(), TIME_FORMAT);
                } else if (diffDay == 1) {
                    return "Yesterday";
                } else if (diffDay <= 7) {
                    return getDate(receivedCalender.getTimeInMillis(), DAY_FORMAT);
                } else {
                    return getDate(receivedCalender.getTimeInMillis(), DATE_FORMAT);
                }
            } else {
                return getDate(receivedCalender.getTimeInMillis(), DATE_FORMAT);
            }
        } else {
            return getDate(receivedCalender.getTimeInMillis(), DATE_FORMAT);
        }

    }
}
