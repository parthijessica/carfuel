package com.ttl.biztextshopper.utilz;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;
import android.webkit.MimeTypeMap;
import android.widget.Toast;

import androidx.fragment.app.FragmentActivity;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.ttl.biztextshopper.R;


import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

public class PathUtilz {

    private static final String TAG = "PATHUTILZ";
    private FragmentActivity activity;

    public static File getPathFromUri(final Context context, final Uri uri) {



        // DocumentProvider
        if ( DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return stringtoFile(Environment.getExternalStorageDirectory() + "/" + split[1]);
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {
                Log.d(TAG, "getPathFromUri: ");
               /* final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return stringtoFile(getDataColumn(context, contentUri, null, null));*/
                String fileName = getFilePath(context, uri);
                if (fileName != null) {
                    return stringtoFile(Environment.getExternalStorageDirectory().toString() + "/Download/" + fileName);
                }

                String id = DocumentsContract.getDocumentId(uri);
                if (id.startsWith("raw:")) {
                    id = id.replaceFirst("raw:", "");
                    File file = new File(id);
                    if (file.exists())
                        return stringtoFile(id);
                }

                final Uri contentUri = ContentUris.withAppendedId(Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
                return stringtoFile(getDataColumn(context, contentUri, null, null));
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                Log.d(TAG, "getPathFromUri: "+type);
                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }else {
                    contentUri = MediaStore.Files.getContentUri("external");
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[] {
                        split[1]
                };

                return stringtoFile(getDataColumn(context, contentUri, selection, selectionArgs));
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri))
                return stringtoFile(uri.getLastPathSegment());

            return stringtoFile(getDataColumn(context, uri, null, null));
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return stringtoFile(uri.getPath());
        }

        return null;
    }

    private static String getFilePath(Context context, Uri uri) {

        Cursor cursor = null;
        final String[] projection = {
                MediaStore.MediaColumns.DISPLAY_NAME
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, null, null,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DISPLAY_NAME);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }
    private static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    private static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    private static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    private static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    private static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }

    private static File stringtoFile(String s){
        File file=null;
        try{
            file = new File(s);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return  file;
    }


    public static void downloadFile(final FragmentActivity activity, final String attachmentUrl) {
        ArrayList<String> perrmissionList = new ArrayList<>();
        perrmissionList.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        perrmissionList.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        if (!isValidUrl(attachmentUrl)) {
            toastShort(activity, "Invalid URL Passed");
            return;
        }
        final String newattachmentUrl = attachmentUrl.replace(" ", "%20");

        try {
            if (PermissionUtilz.hasPermissions(activity, perrmissionList)) {
                fetchFile(activity, newattachmentUrl);
            } else {
                Dexter.withActivity(activity)
                        .withPermissions(perrmissionList)
                        .withListener(new MultiplePermissionsListener() {
                            @Override
                            public void onPermissionsChecked(MultiplePermissionsReport report) {
                                if (report.areAllPermissionsGranted()) {
                                    fetchFile(activity, newattachmentUrl);
                                }
                            }

                            @Override
                            public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                                token.continuePermissionRequest();
                            }
                        }).check();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private static void toastShort(FragmentActivity activity, String invalid_url_passed) {
        try{
            Toast.makeText(activity, invalid_url_passed, Toast.LENGTH_SHORT).show();
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }


    public static void fetchFile(final FragmentActivity activity, final String attachmentUrl) {

        final String fileName = attachmentUrl.substring(attachmentUrl
                .lastIndexOf('/') + 1);
        Log.d(TAG, "fetchFile: " + attachmentUrl);
        if (!openFileIfAvailable(activity, attachmentUrl)) {
            Log.d(TAG, "fetchFile: 11 " + attachmentUrl);
            if (isNetworkAvailable(activity)) {
                Log.d(TAG, "fetchFile: dial" + attachmentUrl);
                new AlertDialog.Builder(activity)
                        .setTitle("Download File")
                        .setMessage(
                                "Are you sure you want to download " + fileName
                                        + "  to your Phone?")
                        .setPositiveButton("Yes, Please",
                                (dialog, which) -> {

                                    if (activity != null) {
                                        new DownloadFileFromURL(activity).execute(attachmentUrl, fileName);
                                    }
                                })
                        .setNegativeButton("No, Thanks",
                                (dialog, which) -> {
                                    // do nothing
                                }).setIcon(android.R.drawable.ic_dialog_alert).show();

            }

        }
    }



    private static boolean openFileIfAvailable(FragmentActivity activity, String attachmentUrl) {

        String fileName = attachmentUrl.substring(attachmentUrl
                .lastIndexOf('/') + 1);
        File f = new File(Environment.getExternalStorageDirectory()
                .getPath()  + "/"+ R.string.app_name+"/"+ fileName);
        if (f.exists()) {
            try {
                toastShort(activity, "Opening downloaded file");

                StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                StrictMode.setVmPolicy(builder.build());

                // Just example, you should parse file name for extension
                String mime = MimeTypeMap.getSingleton()
                        .getMimeTypeFromExtension(
                                getFileExt(fileName));
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.setDataAndType(Uri.fromFile(f), mime);
                activity.startActivity(intent);

                return true;
            } catch (Exception e) {

                e.printStackTrace();
                toastShort(activity, "No application available to open this file of type " + getFileExt(fileName));
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Background Async Task to download file
     */
    public static class DownloadFileFromURL extends AsyncTask<String, String, String> {
        // Progress Dialog
        private ProgressDialog pDialog;
        String fileName, attachmentUrl;
        FragmentActivity activity;

        public DownloadFileFromURL(FragmentActivity activity) {
            this.activity = activity;
        }

        /**
         * Before starting background thread Show Progress Bar Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pDialog = new ProgressDialog(activity);
            pDialog.setMessage("Downloading File. Please wait...");
            pDialog.setIndeterminate(false);
            pDialog.setMax(100);
            pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        /**
         * Downloading file in background thread
         */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                URL url = new URL(f_url[0]);
                attachmentUrl = f_url[0];

                fileName = f_url[1];

                // Log.d("getAttachmentUrl",attachmentUrl+"  "+fileName);

                URLConnection conection = url.openConnection();
                conection.connect();
                // getting file length
                int lenghtOfFile = conection.getContentLength();

                // input stream to read file - with 8k buffer
                InputStream input = new BufferedInputStream(url.openStream(),
                        8192);

                File folder = new File(Environment
                        .getExternalStorageDirectory().getPath()
                        + "/"+R.string.app_name+"/");
                boolean success = true;
                if (!folder.exists()) {
                    success = folder.mkdir();
                }
                if (!success) {
                    // Failed to create Directory
                    return null;
                }
                // Output stream to write file
                OutputStream output = new FileOutputStream(Environment
                        .getExternalStorageDirectory().getPath()
                        + "/"+R.string.app_name+"/"+fileName);

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage() + " " + e);
                return null;
            }

            return "Complete";
        }

        /**
         * Updating progress bar
         */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            pDialog.setProgress(Integer.parseInt(progress[0]));
        }

        /**
         * After completing background task Dismiss the progress dialog
         **/
        @Override
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after the file was downloaded
            pDialog.dismiss();

            if (activity != null) {
                if (file_url != null) {
                    new AlertDialog.Builder(activity)
                            .setTitle("Download Success")
                            .setMessage(
                                    "File Saved Successfully to \n"
                                            + Environment.getExternalStorageDirectory()
                                            .getPath() +   "/"+R.string.app_name+"/"
                                            + fileName + " .Do you want to view the file?")
                            .setPositiveButton("Yes, Please",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int which) {

                                            if (activity != null) {
                                                openFileIfAvailable(activity, attachmentUrl);
                                            }
                                        }
                                    })
                            .setNegativeButton("No, Thanks",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int which) {
                                            // do nothing
                                        }
                                    }).setIcon(android.R.drawable.ic_dialog_alert).show();
                } else
                    toastShort(activity, "Failed to save file");
            }


        }
    }


    private static boolean isNetworkAvailable(Activity activity) {
        try {
            ConnectivityManager cm = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = cm.getActiveNetworkInfo();
            //should check null because in airplane mode it will be null
            return (netInfo != null && netInfo.isConnected());
        } catch (NullPointerException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static String getFileExt(String fileName) {
        return fileName.substring((fileName.lastIndexOf(".") + 1),
                fileName.length());
    }

    public static boolean isValidUrl(String inputUrl) {
        if (!inputUrl.contains("http://"))
            inputUrl = "http://" + inputUrl;

        try {
            new URL(inputUrl);
            return true;
        } catch (MalformedURLException e) {
            Log.v(TAG, "bad url entered");
            return false;
        }

    }

    public static File isAvailableFile(FragmentActivity activity, String attachmentUrl) {

        String fileName = attachmentUrl.substring(attachmentUrl
                .lastIndexOf('/') + 1);
        File f = new File(Environment.getExternalStorageDirectory()
                .getPath() + "/" + R.string.app_name + "/" + fileName);
        return  f;
    }

   public  static  void  openPdfFile(FragmentActivity activity, File file){
       try {
           toastShort(activity, "Opening downloaded file");

           StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
           StrictMode.setVmPolicy(builder.build());

           // Just example, you should parse file name for extension
           String mime = MimeTypeMap.getSingleton()
                   .getMimeTypeFromExtension(
                           getFileExt(file.getName()));
           Intent intent = new Intent();
           intent.setAction(Intent.ACTION_VIEW);
           intent.setDataAndType(Uri.fromFile(file), mime);
           activity.startActivity(intent);

       } catch (Exception e) {
           e.printStackTrace();
           toastShort(activity, "No application available to open this file of type " + getFileExt(file.getName()));
       }
   }

   public static  String   getFileName( String attachmentUrl){
      return  attachmentUrl.substring(attachmentUrl
               .lastIndexOf('/') + 1);
   }
}
