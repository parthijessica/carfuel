package com.ttl.car_mileage.utilz;

import android.content.Context;
import android.content.pm.PackageManager;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;

import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;

import java.util.List;
    public class PermissionUtilz {
        static  String TAG="TAG";
        static boolean isGranted = false;
        static boolean isGoogleLocation = false;
        private  static int REQUEST_CHECK_SETTINGS=8888;

        public static boolean runtimePer(final FragmentActivity activity, final List<String> permissionList, final boolean repeat) {
            Dexter.withActivity(activity)
                    .withPermissions(permissionList)
                    .withListener(new MultiplePermissionsListener() {
                        @Override
                        public void onPermissionsChecked(MultiplePermissionsReport report) {
                            if (report.areAllPermissionsGranted()) {
                                isGranted = true;
                                return;
                            } else {
                                isGranted = false;
                                return;
                            }
                        }

                        @Override
                        public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                            token.continuePermissionRequest();
                        }
                    }).check();
            return isGranted;
        }

        public  static  void  googleLocationPermission(FragmentActivity activity){

            try{
                LocationRequest locationRequest = LocationRequest.create();
                locationRequest.setInterval(1000);
                locationRequest.setFastestInterval(500);
                locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                        .addLocationRequest(locationRequest);
                builder.setAlwaysShow(true);

                Task<LocationSettingsResponse> task = LocationServices.getSettingsClient(activity).checkLocationSettings(builder.build());

                task.addOnSuccessListener(activity, new OnSuccessListener<LocationSettingsResponse>() {
                    @Override
                    public void onSuccess(LocationSettingsResponse locationSettingsResponse) {
                        // All location settings are satisfied. The client can initialize
                        // location requests here.
                        // ...
                        Log.d(TAG, "onSuccess: LOCATION loc " + LocationSettingsResponse.class.getName());
                        isGranted =true;
                    }
                });

                task.addOnFailureListener(activity, new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        if (e instanceof ResolvableApiException) {
                            // Location settings are not satisfied, but this can be fixed
                            // by showing the user a dialog.
                            try {
                                // Show the dialog by calling startResolutionForResult(),
                                // and check the result in onActivityResult().
                                ResolvableApiException resolvable = (ResolvableApiException) e;
//                            ResolvableApiException  resolvable = new ResolvableApiException(new Status(6));
                                resolvable.startResolutionForResult(activity,REQUEST_CHECK_SETTINGS);
                                Log.d(TAG, "onSuccess: LOCATION loc " + resolvable.getStatusCode());
                                isGranted =false;
                            } catch (Exception sendEx) {
                                // Ignore the error.
                            }
                        }
                    }
                });
            }catch (Exception ex){
                ex.printStackTrace();
            }

        }

        public static boolean hasPermissions(Context context, List<String> permissionList) {
            boolean  isGranted =true;
            if (context != null && permissionList != null) {
                for (String permission : permissionList) {
                    if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                        isGranted = false;
                    }

                }
            }
            Log.d(TAG, "onSuccess: LOCATION goog " + isGranted);
            return isGranted;
        }

    }

