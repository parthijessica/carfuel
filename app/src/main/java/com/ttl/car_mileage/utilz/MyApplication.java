package com.ttl.car_mileage.utilz;

import android.app.Application;

import com.ttl.car_mileage.R;

import io.github.inflationx.calligraphy3.CalligraphyConfig;
import io.github.inflationx.calligraphy3.CalligraphyInterceptor;
import io.github.inflationx.viewpump.ViewPump;


public class MyApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        ViewPump.init(ViewPump.builder()
                .addInterceptor(new CalligraphyInterceptor(
                        new CalligraphyConfig.Builder()
                                .setDefaultFontPath("fonts/Proxima-Nova-Regular.otf")
                                .setFontAttrId(R.attr.fontPath)
                                .build()))
                .build());
    }
}
