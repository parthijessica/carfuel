package com.ttl.car_mileage.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.ttl.car_mileage.R;
import com.ttl.car_mileage.interfaces.ClickEventPassValue;
import com.ttl.car_mileage.local_database.common.ShowOurVehicle;
import com.ttl.car_mileage.utilz.MyUtliz;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OurVehicleListAdapter extends RecyclerView.Adapter<OurVehicleListAdapter.ticketListHolder> {

    FragmentActivity activity;
    ArrayList<ShowOurVehicle> ourVehicleArrayList;
    ClickEventPassValue clickEventPassValue ;


    public OurVehicleListAdapter(FragmentActivity activity, ArrayList<ShowOurVehicle> ourVehicleArrayList, ClickEventPassValue clickEventPassValue) {
        this.activity = activity;
        this.ourVehicleArrayList = ourVehicleArrayList;
        this.clickEventPassValue = clickEventPassValue;
    }

    @NonNull
    @Override
    public ticketListHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_our_vehicle, parent, false);
        return new ticketListHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ticketListHolder holder, int position) {
        try {
            ShowOurVehicle dat = ourVehicleArrayList.get(position);
            holder.txtVehicelBrand.setText(dat.getBrandName());
            holder.txtVehicleModels.setText(dat.getModel());
            holder.imgVehiclePic.setImageBitmap(dat.getImage());

            holder.itemView.setOnClickListener(v->{
                clickEventPassValue.OnclickPosition("currentData",String.valueOf(position));
            });
        }catch (Exception  ex){
            ex.printStackTrace();
        }
    }


    @Override
    public int getItemCount() {
        return ourVehicleArrayList.size();
    }

    public static class ticketListHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.img_vehicle_pic)
        AppCompatImageView imgVehiclePic;
        @BindView(R.id.txt_vehicel_brand)
        AppCompatTextView txtVehicelBrand;
        @BindView(R.id.txt_vehicle_models)
        AppCompatTextView txtVehicleModels;
        public ticketListHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
