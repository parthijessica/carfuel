package com.ttl.car_mileage.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.ttl.car_mileage.R;

import java.util.ArrayList;
import java.util.List;


import butterknife.BindView;
import butterknife.ButterKnife;

public class AddChatContactListAdapter extends RecyclerView.Adapter<AddChatContactListAdapter.addChatContactListHolder> {

    Activity activity;
    List<String> list = new ArrayList<>();



    public AddChatContactListAdapter(Activity activity) {

        this.activity = activity;
        list.add("DSK Furnitures");
        list.add("KM Electronics");
        list.add("Roof Shop");
        list.add("KM Electronics");
        list.add("DSK Furnitures");
    }

    @NonNull
    @Override
    public addChatContactListHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_add_chat_contact_list_view, parent, false);
        return new addChatContactListHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull addChatContactListHolder holder, int position) {

    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public class addChatContactListHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.name_txt)
        TextView nameTxt;
        @BindView(R.id.mobile_txt)
        TextView mobileTxt;
        @BindView(R.id.mail_txt)
        TextView mailTxt;
        @BindView(R.id.holder_view)
        CardView holderView;

        public addChatContactListHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
