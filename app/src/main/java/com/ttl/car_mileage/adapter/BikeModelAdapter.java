package com.ttl.car_mileage.adapter;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.ttl.car_mileage.R;
import com.ttl.car_mileage.activity.BikeFuelEntry;
import com.ttl.car_mileage.local_database.bike_database.BikeTable;
import com.ttl.car_mileage.local_database.car_table.CarTable;

import java.util.ArrayList;
import java.util.List;


import butterknife.BindView;
import butterknife.ButterKnife;

public class BikeModelAdapter extends RecyclerView.Adapter<BikeModelAdapter.CarModels> {

        FragmentActivity activity;
        List<BikeTable> carTableList;

        public BikeModelAdapter(FragmentActivity activity, List<BikeTable> carTableList) {
            this.activity = activity;
            this.carTableList = carTableList;
        }

        @NonNull
        @Override
        public  CarModels onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_car_models, parent, false);
            return new CarModels(view);
        }

        @Override
        public void onBindViewHolder(@NonNull CarModels holder, int position) {
            try {
                BikeTable data = carTableList.get(position);
                holder.txtModelName.setText(data.getBikeModel());

                holder.itemView.setOnClickListener(v->{
                    activity.startActivity(new Intent(activity, BikeFuelEntry.class)
                    .putExtra("bikeEntry",data.getBike_br_id())
                    );
                });
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }


        @Override
        public int getItemCount() {
            return carTableList.size();
        }

        public void setList(ArrayList<BikeTable> filterList) {
            carTableList = filterList;
            notifyDataSetChanged();
        }

        public static class CarModels extends RecyclerView.ViewHolder {
            @BindView(R.id.txt_model_name)
            AppCompatTextView txtModelName;
            @BindView(R.id.holder_view)
            CardView holderView;

            public CarModels(@NonNull View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }
        }
    }
