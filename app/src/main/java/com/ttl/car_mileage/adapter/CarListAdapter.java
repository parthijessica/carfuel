package com.ttl.car_mileage.adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;

import com.ttl.car_mileage.R;
import com.ttl.car_mileage.activity.CarModelList;
import com.ttl.car_mileage.local_database.car_table.CarTable;
import com.ttl.car_mileage.utilz.MyUtliz;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CarListAdapter extends RecyclerView.Adapter<CarListAdapter.productListHolder> {

    Activity activity;

    List<CarTable> carTableList;


    public CarListAdapter(Activity activity, List<CarTable> carTableList) {
        this.activity = activity;
        this.carTableList = carTableList;
    }


    @NonNull
    @Override
    public productListHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_car_brands_item, parent, false);
        return new productListHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull productListHolder holder, int position) {
        try{
            CarTable data = carTableList.get(position);

            holder.txtCarBrandName.setText(data.getCarBrand());
            holder.imgBrandIcon.setImageBitmap(MyUtliz.convertByteToBitmap(data.getImage()));

            holder.itemView.setOnClickListener(v->{
                activity.startActivity(new Intent(activity, CarModelList.class).putExtra("brand",data.getCarBrand()));
            });

        }catch (Exception ex){
            ex.printStackTrace();
        }

    }


    @Override
    public int getItemCount() {
        return carTableList.size();
    }

    public void setList(ArrayList<CarTable> filterList) {
        carTableList=filterList;
        notifyDataSetChanged();
    }

    public static class productListHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_brand_icon)
        AppCompatImageView imgBrandIcon;
        @BindView(R.id.txt_car_brand_name)
        AppCompatTextView txtCarBrandName;

        public productListHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
