package com.ttl.car_mileage.adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.ttl.car_mileage.R;
import com.ttl.car_mileage.activity.CarModelList;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SelectShopAdapter extends RecyclerView.Adapter<SelectShopAdapter.selectShopHolder> {

    Activity activity;
    List<String> list = new ArrayList<>();

    public SelectShopAdapter(Activity activity) {

        this.activity = activity;
        list.add("DSK Furnitures");
        list.add("KM Electronics");
        list.add("Roof Shop");
        list.add("KM Electronics");
        list.add("DSK Furnitures");
    }

    @NonNull
    @Override
    public selectShopHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_select_shop_view, parent, false);
        return new selectShopHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull selectShopHolder holder, int position) {
        holder.titleTxt.setText(list.get(position));
        holder.call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        holder.chatIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });

        holder.ticketIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });


        holder.holderView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                activity.startActivity(new Intent(activity, CarModelList.class));
            }
        });

    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public class selectShopHolder extends RecyclerView.ViewHolder {
       @BindView(R.id.title_txt)
        TextView titleTxt;
       @BindView(R.id.ticket_icon)
        ImageView ticketIcon;
       @BindView(R.id.chat_icon)
        ImageView chatIcon;
       @BindView(R.id.call)
        ImageView call;
       @BindView(R.id.holder_view)
        CardView holderView;

        public selectShopHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
