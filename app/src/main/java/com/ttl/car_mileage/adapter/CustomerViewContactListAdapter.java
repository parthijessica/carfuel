package com.ttl.car_mileage.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.ttl.car_mileage.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CustomerViewContactListAdapter extends RecyclerView.Adapter<CustomerViewContactListAdapter.customerViewContactListHolder> {

    Activity activity;
    List<String> list = new ArrayList<>();



    public CustomerViewContactListAdapter(Activity activity) {

        this.activity = activity;
        list.add("DSK Furnitures");
        list.add("KM Electronics");
        list.add("Roof Shop");
        list.add("KM Electronics");
        list.add("DSK Furnitures");
    }

    @NonNull
    @Override
    public customerViewContactListHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_customer_contact_list_view, parent, false);
        return new customerViewContactListHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull customerViewContactListHolder holder, int position) {

    }


    @Override
    public int getItemCount() {
        return list.size();
    }

    public class customerViewContactListHolder extends RecyclerView.ViewHolder {
       @BindView(R.id.img)
        ImageView img;
       @BindView(R.id.name_txt)
        TextView nameTxt;
       @BindView(R.id.mobile_txt)
        TextView mobileTxt;
       @BindView(R.id.mail_txt)
        TextView mailTxt;
       @BindView(R.id.more)
        ImageView more;
       @BindView(R.id.action_close)
        RelativeLayout actionClose;
       @BindView(R.id.action_chat)
        RelativeLayout actionChat;
       @BindView(R.id.holder_view)
        CardView holderView;

        public customerViewContactListHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
