package com.ttl.car_mileage.adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.ttl.car_mileage.R;
import com.ttl.car_mileage.activity.BikeModels;
import com.ttl.car_mileage.local_database.bike_database.BikeTable;
import com.ttl.car_mileage.utilz.MyUtliz;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BikeListAdapter extends RecyclerView.Adapter<BikeListAdapter.bikeHolder> {

    Activity activity;
    List<BikeTable> bikeTableArrayList;

    public BikeListAdapter(Activity activity, List<BikeTable> bikeTableArrayList) {
        this.activity = activity;
        this.bikeTableArrayList = bikeTableArrayList;
    }

    @NonNull
    @Override
    public bikeHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_car_brands_item, parent, false);
        return new bikeHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull bikeHolder holder, int position) {
        try {

            BikeTable data = bikeTableArrayList.get(position);

            holder.txtCarBrandName.setText(data.getBikeBrand());
            holder.imgBrandIcon.setImageBitmap(MyUtliz.convertByteToBitmap(data.getImage()));

            holder.itemView.setOnClickListener(v->{
                activity.startActivity(new Intent(activity, BikeModels.class).putExtra("brand",data.getBikeBrand()));
            });

        }catch (Exception ex){
            ex.printStackTrace();
        }


    }


    @Override
    public int getItemCount() {
        return bikeTableArrayList.size();
    }

    public void setList(ArrayList<BikeTable> filterList) {
        try{
            this.bikeTableArrayList=filterList;
            notifyDataSetChanged();

        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    public static class bikeHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.img_brand_icon)
        AppCompatImageView imgBrandIcon;
        @BindView(R.id.txt_car_brand_name)
        AppCompatTextView txtCarBrandName;

        public bikeHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
