package com.ttl.car_mileage.adapter;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.ttl.car_mileage.R;
import com.ttl.car_mileage.activity.CarFuelEntry;
import com.ttl.car_mileage.local_database.car_table.CarTable;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CarModelsListAdapter extends RecyclerView.Adapter<CarModelsListAdapter.CarModels> {

    FragmentActivity activity;
    List<CarTable> carTableList;


    public CarModelsListAdapter(FragmentActivity activity, List<CarTable> carTableList) {
        this.activity = activity;
        this.carTableList = carTableList;
    }

    @NonNull
    @Override
    public CarModels onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_car_models, parent, false);
        return new CarModels(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CarModels holder, int position) {
        try {
            CarTable data = carTableList.get(position);
            holder.txtModelName.setText(data.getCarModel());

            holder.itemView.setOnClickListener(v->{
                activity.startActivity(new Intent(activity, CarFuelEntry.class)
                    .putExtra("carEntry",new Gson().toJson(data))
                );
            });

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    @Override
    public int getItemCount() {
        return carTableList.size();
    }

    public void setList(ArrayList<CarTable> filterList) {
        carTableList = filterList;
        notifyDataSetChanged();
    }

    public static class CarModels extends RecyclerView.ViewHolder {
        @BindView(R.id.txt_model_name)
        AppCompatTextView txtModelName;
        @BindView(R.id.holder_view)
        CardView holderView;

        public CarModels(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
