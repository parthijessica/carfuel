package com.ttl.car_mileage.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.cardview.widget.CardView;

import com.ttl.car_mileage.R;
import com.ttl.car_mileage.utilz.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddNewCustomerActivity extends BaseActivity {


    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.login_btn)
    CardView loginBtn;
    @BindView(R.id.name_txt)
    AppCompatEditText nameTxt;
    @BindView(R.id.first_name_txt)
    AppCompatEditText firstNameTxt;
    @BindView(R.id.email_txt)
    AppCompatEditText emailTxt;
    @BindView(R.id.user_shop)
    Spinner userShop;
    @BindView(R.id.add_btn)
    Button addBtn;
    @BindView(R.id.bottom_view)
    RelativeLayout bottomView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_customer);
        ButterKnife.bind(this);
        title.setText("Add Customer");
    }

    @OnClick({R.id.back, R.id.add_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back:
                onBackPressed();
                break;
            case R.id.add_btn:
                startActivity(new Intent(this, CarFuelEntry.class));
                break;
        }
    }
}
