package com.ttl.car_mileage.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.ttl.car_mileage.R;
import com.ttl.car_mileage.adapter.CarListAdapter;
import com.ttl.car_mileage.local_database.DatabaseClient;
import com.ttl.car_mileage.local_database.car_table.CarTable;
import com.ttl.car_mileage.local_database.car_table.CarTableDao;
import com.ttl.car_mileage.utilz.BaseActivity;
import com.ttl.car_mileage.utilz.CameraGallery;
import com.ttl.car_mileage.utilz.MyUtliz;
import com.ttl.car_mileage.utilz.PathUtilz;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CarListAcitivty extends BaseActivity {

    private static final String TAG = "carTable";
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.login_btn)
    CardView loginBtn;
    @BindView(R.id.search)
    AppCompatEditText search;
    @BindView(R.id.recy_car_list)
    RecyclerView recyCarList;
    @BindView(R.id.fbtn_add_brand)
    FloatingActionButton fbtnAddBrand;
    private CarTableDao carTableDB;
    private ImageView imgCancel;
    private TextView txtImage;
    private ImageView imgcarBrand;
    private TextView txSubmitDia;
    private EditText edtMileage, edtFuelCapacity, edtCarModel, edtCarBrand;
    private Dialog carBrandDialog;
    private Spinner spnFuelType;
    private File selectedCarImage;

    private ArrayList<String> spnList;

    private ArrayList<CarTable> carTableList;
    private List<CarTable> carList;
    private CarListAdapter carListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_list);
        ButterKnife.bind(this);
        title.setText("Car Brands");
        spnList = new ArrayList<>();
        spnList.add("Petrol");
        spnList.add("Diesel");
        /**
         * initialization DB Object  for Car Brand
         */
        carTableDB = DatabaseClient.getInstance(this).getAppDatabase().carTableDao();
        carTableList = new ArrayList<>();
        recyCarList.setLayoutManager(new GridLayoutManager(this, 3));

        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (carList!=null &&carList.size()!=0) {
                    ArrayList<CarTable> filterList =new ArrayList<>();
                    for (CarTable carTable : carList) {
                        if (carTable.getCarBrand().trim().toLowerCase().contains(s.toString().toLowerCase().trim())) {
                            filterList.add(carTable);
                        }
                    }
                    carListAdapter.setList(filterList);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        getAllcarBrands();
    }


    private void getAllcarBrands() {
        MyUtliz.showLoading(this,false);

       class  getCheck extends AsyncTask<Void,Void,Void>{
           @Override
           protected Void doInBackground(Void... voids) {
               carList =carTableDB.getBrandName();
               return null;
           }

           @Override
           protected void onPostExecute(Void aVoid) {
               super.onPostExecute(aVoid);
               if (carList!=null && carList.size()!=0) {
                   setAdapter();
               }else {
                   new Handler().postDelayed(()->{saveCar();},1000);
               }
           }
       }

       new getCheck().execute();

    }

    @OnClick({R.id.back, R.id.fbtn_add_brand, R.id.login_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back:
                finish();
                break;
            case R.id.fbtn_add_brand:
                digAddcarBrand();
                break;
            case R.id.login_btn:
                break;
        }
    }

    private void digAddcarBrand() {
        try {

            carBrandDialog = new Dialog(this);
            carBrandDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            carBrandDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            carBrandDialog.setContentView(R.layout.dialog_add_show_car_brand);
            carBrandDialog.show();
            imgCancel = carBrandDialog.findViewById(R.id.img_cancel);
            txtImage = carBrandDialog.findViewById(R.id.txt_image);
            imgcarBrand = carBrandDialog.findViewById(R.id.img_car_brand);
            txSubmitDia = carBrandDialog.findViewById(R.id.txt_submit_dia);
            edtMileage = carBrandDialog.findViewById(R.id.edt_mileage);
            spnFuelType = carBrandDialog.findViewById(R.id.spn_fuel_type);
            edtFuelCapacity = carBrandDialog.findViewById(R.id.edt_fuel_capacity);
            edtCarModel = carBrandDialog.findViewById(R.id.edt_car_model);
            edtCarBrand = carBrandDialog.findViewById(R.id.edt_car_brand);

            spnFuelType.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, spnList));

            imgCancel.setOnClickListener(m -> {
                carBrandDialog.dismiss();
            });

            txSubmitDia.setOnClickListener(v -> {

                String carBrand = edtCarBrand.getText().toString();
                String carModel = edtCarModel.getText().toString();
                String fuelCap = edtFuelCapacity.getText().toString();
                String mileage = edtMileage.getText().toString();
                String fuelType = spnFuelType.getSelectedItem().toString();

                if (validation(carBrand, carModel, fuelCap, mileage)) {
                    if (selectedCarImage != null) {
                        saveLocalCarTable(carBrand, carModel, fuelCap, mileage, fuelType);
                    } else {
                        MyUtliz.ShowToast(this, "Selected Image");
                    }
                }

            });
            txtImage.setOnClickListener(v -> {
                CameraGallery.showSelectCameraGellery(this, false);
            });


        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private boolean validation(String carBrand, String carModel, String fuelCap, String mileage) {
        if (carBrand.length() == 0) {
            MyUtliz.ShowToast(this, "Car Brand Can't Be Empty");
            return false;
        } else if (carModel.length() == 0) {
            MyUtliz.ShowToast(this, "Car Model Can't Be Empty");
            return false;
        } else if (fuelCap.length() == 0) {
            MyUtliz.ShowToast(this, "Fuel Capacity  Can't Be Empty");
            return false;
        } else if (mileage.length() == 0) {
            MyUtliz.ShowToast(this, "Mileage  Can't Be Empty");
            return false;
        } else {
            return true;
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CameraGallery.REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK && data != null) {
            if (data.getData() != null) {
                Bundle extras = data.getExtras();
                Bitmap imageBitmap = (Bitmap) extras.get("data");
                File file = MyUtliz.fileFolderCreated("ApplicationLocalData", Calendar.getInstance().getTimeInMillis() + ".png");
                selectedCarImage = MyUtliz.conBitmapToFile(imageBitmap, file);
                Log.d(TAG, "onActivityResult: " + selectedCarImage.getName());

            }
        } else if (requestCode == CameraGallery.SELECT_IMAGE && resultCode == Activity.RESULT_OK && data != null) {
            selectedCarImage = PathUtilz.getPathFromUri(this, data.getData());
            Log.d(TAG, "onActivityResult: " + selectedCarImage.getName());
        }
        if (selectedCarImage != null) {
            imgcarBrand.setVisibility(View.VISIBLE);
            MyUtliz.loadfiletoIMG(selectedCarImage, imgcarBrand);
        }
    }

    private void saveLocalCarTable(String carBrand, String carModel, String fuelCap, String mileage, String fuelType) {
        try {
            CarTable carTable = new CarTable(
                    carBrand,
                    carModel,
                    mileage,
                    fuelCap, fuelType, MyUtliz.convertFileToBytes(selectedCarImage)
            );

            class AddCarBranData extends AsyncTask<Void, Void, Void> {
                CarTable data;
                boolean isSucesss = false;

                @Override
                protected Void doInBackground(Void... voids) {
                    data = carTableDB.checkCarBrand(carBrand, carModel);
                    if (data != null) {
                        isSucesss = false;
                    } else {
                        isSucesss = true;
                        carTableDB.insert(carTable);
                        carList.clear();
                        carList =carTableDB.getBrandName();
                    }

                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    if (isSucesss) {
                        if (carBrandDialog != null && carBrandDialog.isShowing()) {
                            carBrandDialog.dismiss();
                        }
                        selectedCarImage = null;
                        carListAdapter=  new CarListAdapter(CarListAcitivty.this,carList);
                        recyCarList.setAdapter(carListAdapter);
                        MyUtliz.ShowToast(CarListAcitivty.this, "Car Brand Added SuccessFully");
                    } else {
                        MyUtliz.ShowToast(CarListAcitivty.this, "Invalid Data Brand and Model");
                    }
                }
            }
            new AddCarBranData().execute();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void saveCar() {
        try {


            carTableList.add(new CarTable("Audi", "Audi A6", "20", "15", "Petrol", MyUtliz.convetDrawble(getDrawable(R.drawable.audi))));
            carTableList.add(new CarTable("Audi", "Audi A8L", "20", "15", "Petrol", MyUtliz.convetDrawble(getDrawable(R.drawable.audi))));
            carTableList.add(new CarTable("Audi", "Audi Q8", "20", "15", "Petrol", MyUtliz.convetDrawble(getDrawable(R.drawable.audi))));
            carTableList.add(new CarTable("Audi", "Audi Q7", "20", "15", "Petrol", MyUtliz.convetDrawble(getDrawable(R.drawable.audi))));
            carTableList.add(new CarTable("Audi", "Audi A4", "20", "15", "Petrol", MyUtliz.convetDrawble(getDrawable(R.drawable.audi))));


            carTableList.add(new CarTable("Mercedes-Benz", "C -CLASS", "20", "15", "Petrol", MyUtliz.convetDrawble(getDrawable(R.drawable.benz))));
            carTableList.add(new CarTable("Mercedes-Benz", "S -CLASS", "20", "15", "Petrol", MyUtliz.convetDrawble(getDrawable(R.drawable.benz))));
            carTableList.add(new CarTable("Mercedes-Benz", "E -CLASS", "20", "15", "Petrol", MyUtliz.convetDrawble(getDrawable(R.drawable.benz))));
            carTableList.add(new CarTable("Mercedes-Benz", "G -CLASS", "20", "15", "Petrol", MyUtliz.convetDrawble(getDrawable(R.drawable.benz))));
            carTableList.add(new CarTable("Mercedes-Benz", "GLC -CLASS", "20", "15", "Petrol", MyUtliz.convetDrawble(getDrawable(R.drawable.benz))));


            carTableList.add(new CarTable("BMW", "BMW X2", "20", "15", "Petrol", MyUtliz.convetDrawble(getDrawable(R.drawable.bmw))));
            carTableList.add(new CarTable("BMW", "BMW Z1", "20", "15", "Petrol", MyUtliz.convetDrawble(getDrawable(R.drawable.bmw))));
            carTableList.add(new CarTable("BMW", "BMW X7", "20", "15", "Petrol", MyUtliz.convetDrawble(getDrawable(R.drawable.bmw))));
            carTableList.add(new CarTable("BMW", "BMW Z6", "20", "15", "Petrol", MyUtliz.convetDrawble(getDrawable(R.drawable.bmw))));
            carTableList.add(new CarTable("BMW", "BMW X5", "20", "15", "Petrol", MyUtliz.convetDrawble(getDrawable(R.drawable.bmw))));


            carTableList.add(new CarTable("FERRARI", "F430 Gran  Turismo", "20", "15", "Petrol", MyUtliz.convetDrawble(getDrawable(R.drawable.ferrari))));
            carTableList.add(new CarTable("FERRARI", "F360 Sports Prototype", "20", "15", "Petrol", MyUtliz.convetDrawble(getDrawable(R.drawable.ferrari))));


            carTableList.add(new CarTable("VOLVO", "volvo X60", "20", "15", "Petrol", MyUtliz.convetDrawble(getDrawable(R.drawable.volvo))));
            carTableList.add(new CarTable("VOLVO", "volvo X80", "20", "15", "Petrol", MyUtliz.convetDrawble(getDrawable(R.drawable.volvo))));
            carTableList.add(new CarTable("VOLVO", "volvo X90", "20", "15", "Petrol", MyUtliz.convetDrawble(getDrawable(R.drawable.volvo))));

            carTableList.add(new CarTable("Ford", "Figo", "20", "15", "Petrol", MyUtliz.convetDrawble(getDrawable(R.drawable.ford))));
            carTableList.add(new CarTable("Ford", "Aspire", "20", "15", "Petrol", MyUtliz.convetDrawble(getDrawable(R.drawable.ford))));
            carTableList.add(new CarTable("Ford", "Mustang", "20", "15", "Petrol", MyUtliz.convetDrawble(getDrawable(R.drawable.ford))));

            carTableList.add(new CarTable("Suzuki", "Swift", "20", "15", "Petrol", MyUtliz.convetDrawble(getDrawable(R.drawable.suzuki))));
            carTableList.add(new CarTable("Honda", "Amaze", "20", "15", "Petrol", MyUtliz.convetDrawble(getDrawable(R.drawable.honda))));


            class defaultCarList extends AsyncTask<Void, Void, Void> {
                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                }

                @Override
                protected Void doInBackground(Void... voids) {
                    if (carTableDB.getAll().size() != 0) {
                        carTableDB.truncateCarTable();
                    }
                    carTableDB.insertAll(carTableList);
                    carList = carTableDB.getBrandName();
                    Log.d(TAG, "doInBackground: " +carList.size());
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    setAdapter();
                }
            }

            new defaultCarList().execute();

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    private void setAdapter() {
        try {
            carListAdapter=  new CarListAdapter(CarListAcitivty.this,carList);
            recyCarList.setAdapter(carListAdapter);
            MyUtliz.loadingDismiss();
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
}
