package com.ttl.car_mileage.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.cardview.widget.CardView;

import com.ttl.car_mileage.R;
import com.ttl.car_mileage.local_database.DatabaseClient;
import com.ttl.car_mileage.local_database.user.UserTable;
import com.ttl.car_mileage.local_database.user.UserTableDao;
import com.ttl.car_mileage.utilz.BaseActivity;
import com.ttl.car_mileage.utilz.MyUtliz;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignUpActivity extends BaseActivity {


    private static final String TAG = "SignUpActivity";
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.login_btn)
    CardView loginBtn;
    @BindView(R.id.email_txt)
    AppCompatEditText emailTxt;
    @BindView(R.id.first_name_txt)
    AppCompatEditText firstNameTxt;
    @BindView(R.id.phone_txt)
    AppCompatEditText phoneTxt;
    @BindView(R.id.password_txt)
    AppCompatEditText passwordTxt;
    @BindView(R.id.next_btn)
    Button nextBtn;
    private int previousListCount = 0;
    private UserTableDao userDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);
        title.setText("Sign Up");
        /**
         * Object initialing Local Database userDB
         */
        userDB = DatabaseClient.getInstance(SignUpActivity.this).getAppDatabase().userTableDao();

    }


    @OnClick({R.id.back, R.id.title, R.id.next_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back:
                break;
            case R.id.title:
                break;
            case R.id.next_btn:
                String uaername = firstNameTxt.getText().toString();
                String phoneNumber = phoneTxt.getText().toString();
                String emailId = emailTxt.getText().toString();
                String password = passwordTxt.getText().toString();
                if (validation(uaername, phoneNumber, emailId, password)) {
                    storeLocalDatabase(uaername, phoneNumber, emailId, password);
                }

                break;
        }
    }

    private void storeLocalDatabase(String uaername, String phoneNumber, String emailId, String password) {
        try {

            UserTable userTable = new UserTable(uaername, emailId, phoneNumber, password);
            class Registration extends AsyncTask<Void, Void, Void> {
                UserTable data = null;
                boolean  isSuccess =false;
                @Override
                protected Void doInBackground(Void... voids) {
                    data = userDB.verifycationEmailPhone(phoneNumber, emailId);
                    if (data != null) {
                        isSuccess=false;
                    } else {
                        isSuccess=true;
                        userDB.insert(userTable);

                    }
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    if (isSuccess) {
                        MyUtliz.ShowToast(SignUpActivity.this, "Register User Successfully");
                        startActivity(new Intent(SignUpActivity.this,LoginActivity.class));
                    }else {
                        MyUtliz.ShowToast(SignUpActivity.this, "Already exist this user data");

                    }
                }

            }
            Registration check = new Registration();
            check.execute();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private boolean validation(String uaername, String phoneNumber, String emailId, String password) {
        if (uaername.length() == 0) {
            showToast("UserName Can't be Empty");
            return false;
        } else if (phoneNumber.length() != 10) {
            showToast("Phone Number Invalid number");
            return false;
        } else if (emailId.length() == 0) {
            showToast("Email Can't be Empty");
            return false;
        } else if (password.length() == 0) {
            showToast("password Can't be Empty");
            return false;
        } else {
            return true;
        }
    }

    private void showToast(String s) {
        try {
            Toast.makeText(this, s, Toast.LENGTH_SHORT).show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
