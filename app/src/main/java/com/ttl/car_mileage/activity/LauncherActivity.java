package com.ttl.car_mileage.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import androidx.cardview.widget.CardView;

import com.ttl.car_mileage.R;
import com.ttl.car_mileage.utilz.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LauncherActivity extends BaseActivity {


    @BindView(R.id.sign_up_with_email)
    CardView signUpWithEmail;
    @BindView(R.id.sign_up_with_google)
    CardView signUpWithGoogle;
    @BindView(R.id.sign_up_with_facebook)
    CardView signUpWithFacebook;
    @BindView(R.id.sign_in)
    LinearLayout signIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.sign_up_with_email, R.id.sign_up_with_google, R.id.sign_up_with_facebook, R.id.sign_in})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.sign_up_with_email:
                startActivity(new Intent(getContext(), LoginActivity.class));
                break;
            case R.id.sign_up_with_google:
                break;
            case R.id.sign_up_with_facebook:
                break;
            case R.id.sign_in:
                startActivity(new Intent(getContext(), LoginActivity.class));
                break;
        }
    }
}
