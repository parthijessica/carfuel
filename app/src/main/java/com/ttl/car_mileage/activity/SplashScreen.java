package com.ttl.car_mileage.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;

import com.ttl.car_mileage.R;
import com.ttl.car_mileage.local_database.DatabaseClient;
import com.ttl.car_mileage.local_database.car_table.CarTable;
import com.ttl.car_mileage.local_database.car_table.CarTableDao;
import com.ttl.car_mileage.utilz.BaseActivity;
import com.ttl.car_mileage.utilz.LocalSession;

import java.util.ArrayList;
import java.util.List;

public class SplashScreen extends BaseActivity {

    private CarTableDao carTableDB;

    private List<CarTable> carTableList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        final Handler handler = new Handler();
        handler.postDelayed(() -> {
            //Do something after 100ms
            finish();
            if (LocalSession.getBoolean(SplashScreen.this,LocalSession.isLogged,false)) {
                startActivity(new Intent(getContext(), MainActivity.class));
            }else {
                startActivity(new Intent(getContext(), LoginActivity.class));
            }
        }, 2000);

    }


}
