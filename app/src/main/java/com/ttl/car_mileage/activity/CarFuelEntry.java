package com.ttl.car_mileage.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatRadioButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.cardview.widget.CardView;

import com.google.android.material.textfield.TextInputEditText;
import com.google.gson.Gson;
import com.ttl.car_mileage.R;
import com.ttl.car_mileage.local_database.DatabaseClient;
import com.ttl.car_mileage.local_database.car_table.CarTable;
import com.ttl.car_mileage.local_database.fuel_entry.FuelEntry;
import com.ttl.car_mileage.local_database.fuel_entry.FuelFillsTableDao;
import com.ttl.car_mileage.utilz.BaseActivity;
import com.ttl.car_mileage.utilz.MyUtliz;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CarFuelEntry extends BaseActivity {


    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.login_btn)
    CardView loginBtn;
    @BindView(R.id.txt_car_brand_name)
    AppCompatTextView txtCarBrandName;
    @BindView(R.id.txt_car_model_name)
    AppCompatTextView txtCarModelName;
    @BindView(R.id.txt_fuel_type)
    AppCompatTextView txtFuelType;
    @BindView(R.id.txt_fuel_capacity)
    AppCompatTextView txtFuelCapacity;
    @BindView(R.id.txt_mileage)
    AppCompatTextView txtMileage;
    @BindView(R.id.rbtn_60_below)
    AppCompatRadioButton rbtn60Below;
    @BindView(R.id.rbtn_60_100)
    AppCompatRadioButton rbtn60100;
    @BindView(R.id.rbtn_60_above)
    AppCompatRadioButton rbtn60Above;
    @BindView(R.id.edt_fuel_fills)
    TextInputEditText edtFuelFills;
    @BindView(R.id.txt_submit)
    AppCompatTextView txtSubmit;
    private CarTable carTable;
    private FuelFillsTableDao fuelEntryDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activty_car_entry);
        ButterKnife.bind(this);
        title.setText("Car Fuel Entry");
        fuelEntryDB = DatabaseClient.getInstance(this).getAppDatabase().fuelFillsTableDao();
        if (getIntent().hasExtra("carEntry")) {
            carTable = new Gson().fromJson(getIntent().getStringExtra("carEntry"), CarTable.class);
            setData();
        }

        edtFuelFills.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @SuppressLint("SetTextI18n")
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
               double mi = Double.parseDouble(carTable.getMilaege());
                if (s!=null && s.length()!=0) {
                    rbtn60Below.setText(mi * (Double.parseDouble(s.toString())) +" \t mi");
                    rbtn60100.setText((mi - (mi * 0.3)) * (Double.parseDouble(s.toString())) +" \t mi");
                    rbtn60Above.setText((mi - (mi * 0.5)) * (Double.parseDouble(s.toString())) +" \t mi");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void setData() {
        try {
            txtCarBrandName.setText(carTable.getCarBrand());
            txtCarModelName.setText(carTable.getCarModel());
            txtFuelCapacity.setText(carTable.getFuelCap());
            txtFuelType.setText(carTable.getFuelType());
            txtMileage.setText(carTable.getMilaege());
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    @OnClick({R.id.back, R.id.txt_submit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back:
                finish();
                break;
            case R.id.txt_submit:
                String fillsValue =edtFuelFills.getText().toString();
                if (fillsValue.length()!=0) {
                   saveData(fillsValue);
                }else {
                    MyUtliz.ShowToast(this,"Enter Fuel Fills Value");
                }
                break;
        }
    }

    private void saveData(String fillsValue) {
        try {
            String drivingSpeed="";
            if (rbtn60Below.isChecked()) {
                drivingSpeed=rbtn60Below.getText().toString();
            }else if (rbtn60100.isChecked()) {
                drivingSpeed=rbtn60100.getText().toString();
            }else {
                drivingSpeed=rbtn60Above.getText().toString();
            }

            FuelEntry fuelEntry = new FuelEntry(
                    carTable.getCar_br_id(),
                    "CAR",
                    Calendar.getInstance().getTimeInMillis(),
                    fillsValue,
                    drivingSpeed
            );

            class saveFuel extends AsyncTask<Void,Void,Void>{

                @Override
                protected Void doInBackground(Void... voids) {
                    fuelEntryDB.insert(fuelEntry);
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    MyUtliz.ShowToast(CarFuelEntry.this,"Added SuccessFully");
                    finishAffinity();
                    startActivity(new Intent(getApplicationContext(),MainActivity.class));

                }
            }

            new saveFuel().execute();
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
}
