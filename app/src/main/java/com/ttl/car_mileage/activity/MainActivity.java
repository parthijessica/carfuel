package com.ttl.car_mileage.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;

import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.textfield.TextInputEditText;
import com.ttl.car_mileage.R;
import com.ttl.car_mileage.adapter.OurVehicleListAdapter;
import com.ttl.car_mileage.interfaces.ClickEventPassValue;
import com.ttl.car_mileage.local_database.DatabaseClient;
import com.ttl.car_mileage.local_database.bike_database.BikeTable;
import com.ttl.car_mileage.local_database.bike_database.BikeTableDao;
import com.ttl.car_mileage.local_database.car_table.CarTable;
import com.ttl.car_mileage.local_database.car_table.CarTableDao;
import com.ttl.car_mileage.local_database.common.ShowOurVehicle;
import com.ttl.car_mileage.local_database.distance_.TravelTable;
import com.ttl.car_mileage.local_database.distance_.TravelTableDao;
import com.ttl.car_mileage.local_database.fuel_entry.FuelEntry;
import com.ttl.car_mileage.local_database.fuel_entry.FuelFillsTableDao;
import com.ttl.car_mileage.service.LocationCaptureService;
import com.ttl.car_mileage.utilz.BaseActivity;
import com.ttl.car_mileage.utilz.MyUtliz;
import com.ttl.car_mileage.utilz.PermissionUtilz;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.ttl.car_mileage.service.LocationCaptureService.INTENT_FETCH_STR;

public class MainActivity extends BaseActivity implements ClickEventPassValue {


    private static final String TAG = "MainActivity";
    @BindView(R.id.top_layout)
    AppBarLayout topLayout;
    @BindView(R.id.txt_vehicel_brand)
    TextInputEditText txtVehicelBrand;
    @BindView(R.id.txt_vehicle_models)
    TextInputEditText txtVehicleModels;
    @BindView(R.id.txt_fuel_capacity)
    TextInputEditText txtFuelCapacity;
    @BindView(R.id.txt_vehicle_type)
    TextInputEditText txtVehicleType;
    @BindView(R.id.txt_distance)
    TextInputEditText txtDistance;
    @BindView(R.id.txt_speed)
    TextInputEditText txtSpeed;
    @BindView(R.id.txt_source_location)
    TextInputEditText txtSourceLocation;
    @BindView(R.id.txt_designation)
    TextInputEditText txtDesignation;
    @BindView(R.id.lly_bottam)
    LinearLayoutCompat llyBottam;
    @BindView(R.id.lly_car)
    LinearLayoutCompat llyCar;
    @BindView(R.id.lly_bike)
    LinearLayoutCompat llyBike;
    @BindView(R.id.txt_start_location)
    AppCompatTextView txtStartLocation;


    private ArrayList<String> permisionList;
    private FuelFillsTableDao fuelFillsDao;
    private List<FuelEntry> fuelFillsList;
    private ImageView imgCancel;
    private RecyclerView recyOurVehicle;
    private AppCompatTextView txtNoDataFound;
    private CarTableDao carDao;
    private BikeTableDao bikeDao;

    private ArrayList<ShowOurVehicle> ourVehicleArrayList;
    private int pos = 0;
    /**
     * get  last known  location
     */
    private FusedLocationProviderClient fusedLocationClient;
    private double sourceLatitute, designationLatitute;
    private double soureLongitute, designationLongitute;
    private Dialog ourVehicleDialog;
    private float[] distanceArry = new float[5];

    private BroadcastReceiver receiver;
    private TravelTableDao travelDao;
    private List<TravelTable> travelList;
    private Intent locationService;
    private TravelTable lastFetchingData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        /**
         * initialzing get location object
         */
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        permisionList = new ArrayList<>();
        ourVehicleArrayList = new ArrayList<>();
        fuelFillsDao = DatabaseClient.getInstance(this).getAppDatabase().fuelFillsTableDao();
        bikeDao = DatabaseClient.getInstance(this).getAppDatabase().bikeTableDao();
        carDao = DatabaseClient.getInstance(this).getAppDatabase().carTableDao();
        travelDao = DatabaseClient.getInstance(this).getAppDatabase().travelTableDao();

        AsyncTask.execute(() -> {
            fuelFillsList = fuelFillsDao.getAll();
        });


        /**get  location and speed from  service  class  to here using receiver*/

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                updateUI(intent);
            }
        };

        /**fetch last data*/
        fetchData();

    }

    private void fetchData() {
        try {

            class fetchData extends AsyncTask<Void, Void, Void> {

                @Override
                protected Void doInBackground(Void... voids) {
                    travelList = travelDao.getAll();
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    if (travelList != null && travelList.size() != 0) {
                        lastFetchingData = travelList.get(travelList.size() - 1);
                        txtDesignation.setText(lastFetchingData.getDesignation());
                        txtDistance.setText(lastFetchingData.getTravel_distance());
                        txtFuelCapacity.setText(lastFetchingData.getFuelCap());
                        txtSourceLocation.setText(lastFetchingData.getSource());
                        txtVehicelBrand.setText(lastFetchingData.getVehicleBrand());
                        txtVehicleModels.setText(lastFetchingData.getVehicleModel());
                        txtVehicleType.setText(lastFetchingData.getVehicleType());
                        txtSpeed.setText(lastFetchingData.getTravelSpeed());
                    }
                }
            }
            new fetchData().execute();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @SuppressLint("SetTextI18n")
    private void updateUI(Intent intent) {
        try {
            if (intent.hasExtra("latitude")) {
                designationLatitute = Double.parseDouble(intent.getStringExtra("latitude"));
            }
            if (intent.hasExtra("longitude")) {
                designationLongitute = Double.parseDouble(intent.getStringExtra("longitude"));
            }
            if (intent.hasExtra("speed")) {
                Log.d(TAG, "updateUI: " + intent.getStringExtra("speed"));
                txtSpeed.setText(intent.getStringExtra("speed"));
            }
            if (sourceLatitute != 0 && soureLongitute != 0 && designationLongitute != 0 && designationLatitute != 0) {
                Location.distanceBetween(sourceLatitute, soureLongitute, designationLatitute, designationLongitute, distanceArry);
                if (distanceArry != null && distanceArry.length > 0) {
                    txtDistance.setText(distanceArry[0] + "meter");
                    float fuelMil = Float.parseFloat(ourVehicleArrayList.get(pos).getFuelCap()) * 1000;
                    float milMeter = Float.parseFloat(ourVehicleArrayList.get(pos).getMileage().replace("mi", "").trim().replace(".0", "").trim()) * 1000;
                    float dis = distanceArry[0];
                    Log.d(TAG, "updateUI: " + ((fuelMil / milMeter) * dis));
                    txtSpeed.setText(((fuelMil / milMeter) * dis) + "\t ML");
                }
                fusedLocationClient.getLastLocation()
                        .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                            @Override
                            public void onSuccess(Location location) {
                                // Got last known location. In some rare situations this can be null.
                                if (location != null) {
                                    txtDesignation.setText(MyUtliz.getAddress(MainActivity.this, location.getLatitude(), location.getLongitude()).getAddressLine(0));
                                }
                            }
                        });
            }


        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @OnClick({R.id.lly_car, R.id.txt_start_location, R.id.lly_bike})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.lly_car:
                startActivity(new Intent(this, CarListAcitivty.class));
                break;
            case R.id.lly_bike:
                startActivity(new Intent(this, BikeListActivity.class));
                break;
            case R.id.txt_start_location: {
                LocationCaptureService.stopThread();
                if (txtStartLocation.getText().toString().toLowerCase().trim().contains("Stop".toLowerCase())) {
                    txtStartLocation.setText("Start Travelling");
                    LocationCaptureService.stopThread();
                    stopService(locationService);
                    storeTravelData();
                } else {
                    if (fuelFillsList != null && fuelFillsList.size() != 0) {
                        permisionList.add(Manifest.permission.ACCESS_COARSE_LOCATION);
                        permisionList.add(Manifest.permission.ACCESS_FINE_LOCATION);
                        if (PermissionUtilz.hasPermissions(getContext(), permisionList)) {
                            if (MyUtliz.isLocationEnabled(this)) {
                                getVehicleList();

                            } else {
                                PermissionUtilz.googleLocationPermission(this);
                            }
                        } else {
                            PermissionUtilz.runtimePer(this, permisionList, true);
                        }
                    } else {
                        MyUtliz.ShowToast(this, "Entry car  or  bike  which  vehicle  you have ?");
                    }
                }
            }
            break;
        }
    }

    private void storeTravelData() {
        try {
            TravelTable travelTable = new TravelTable(
                    txtDistance.getText().toString(),
                    txtSpeed.getText().toString(),
                    txtSourceLocation.getText().toString(),
                    txtDesignation.getText().toString(),
                    txtVehicleType.getText().toString(),
                    txtVehicelBrand.getText().toString(),
                    txtVehicleModels.getText().toString(),
                    txtFuelCapacity.getText().toString(),
                    ourVehicleArrayList.get(pos).getId(),
                    Calendar.getInstance().getTimeInMillis()
            );

            class storeTravelData extends AsyncTask<Void, Void, Void> {


                @Override
                protected Void doInBackground(Void... voids) {
                    travelDao.insert(travelTable);

                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    MyUtliz.ShowToast(MainActivity.this, "Added SuccessFully");
                }
            }

            new storeTravelData().execute();

        } catch (Exception eex) {
            eex.printStackTrace();
        }
    }

    private void showOurVehicleDialog() {
        try {
            ourVehicleDialog = new Dialog(this);
            ourVehicleDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            ourVehicleDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            ourVehicleDialog.setContentView(R.layout.dialog_show_vehicle);
            ourVehicleDialog.show();
            imgCancel = ourVehicleDialog.findViewById(R.id.img_cancel);
            recyOurVehicle = ourVehicleDialog.findViewById(R.id.recy_our_vehicle);
            txtNoDataFound = ourVehicleDialog.findViewById(R.id.txt_no_data_found);
            recyOurVehicle.setLayoutManager(new LinearLayoutManager(this));
            recyOurVehicle.setAdapter(new OurVehicleListAdapter(this, ourVehicleArrayList, this));
            imgCancel.setOnClickListener(u -> {
                ourVehicleDialog.dismiss();
            });

            Log.d(TAG, "onPostExecute: Fuel " + fuelFillsList.size());
            Log.d(TAG, "onPostExecute: Current " + ourVehicleArrayList.size());

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void getVehicleList() {
        try {

            class getVehicle extends AsyncTask<Void, Void, Void> {

                ProgressDialog progressDialog;

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();
                    progressDialog = new ProgressDialog(MainActivity.this);
                    progressDialog.setIndeterminate(true);
                    progressDialog.setMessage("Loading...");
                    progressDialog.show();
                    ourVehicleArrayList.clear();
                }

                @Override
                protected Void doInBackground(Void... voids) {
                    for (FuelEntry fuelEntry : fuelFillsList) {
                        if (fuelEntry.getVehicleType().trim().toLowerCase().contains("bike")) {
                            BikeTable bikeTable = bikeDao.getBikeData(fuelEntry.getRefID());
                            ourVehicleArrayList.add(new ShowOurVehicle(
                                    MyUtliz.convertByteToBitmap(bikeTable.getImage()),
                                    bikeTable.getBikeBrand(),
                                    bikeTable.getBikeModel(),
                                    bikeTable.getBike_br_id(), "BIKE", bikeTable.getFuelCap(), fuelEntry.getDrvingSpeed()
                            ));
                        } else {
                            CarTable carData = carDao.getCarData(fuelEntry.getRefID());
                            ourVehicleArrayList.add(new ShowOurVehicle(
                                    MyUtliz.convertByteToBitmap(carData.getImage()),
                                    carData.getCarBrand(),
                                    carData.getCarModel(),
                                    carData.getCar_br_id(), "CAR", carData.getFuelCap(), fuelEntry.getDrvingSpeed()
                            ));
                        }
                    }
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    progressDialog.dismiss();
                    Log.d(MainActivity.TAG, "onPostExecute: Fuel " + fuelFillsList.size());
                    Log.d(MainActivity.TAG, "onPostExecute: Current " + ourVehicleArrayList.size());
                    if (ourVehicleArrayList != null && ourVehicleArrayList.size() != 0) {
                        showOurVehicleDialog();
                    }
                }
            }
            new getVehicle().execute();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void OnclickPosition(String name, String position) {
        try {
            Log.d(TAG, "OnclickPosition:  Start Service");
            pos = Integer.parseInt(position);
            switch (name) {
                case "currentData": {
                    if (lastFetchingData != null) {
                        if (MyUtliz.getDate(lastFetchingData.getCreatedAt(), "ddMMyyyy").equals(MyUtliz.getDate(Calendar.getInstance().getTimeInMillis(), "ddMMyyyy"))) {

                            if (lastFetchingData.getVehicleType().toLowerCase().equals("bike")) {
                                startActivity(new Intent(this, BikeModels.class).putExtra("brand", lastFetchingData.getVehicleBrand()));
                            } else {
                                startActivity(new Intent(this, CarModelList.class).putExtra("brand", lastFetchingData.getVehicleBrand()));
                            }

                        } else {
                            txtStartLocation.setText("Stop Travelling");
                            txtVehicelBrand.setText(ourVehicleArrayList.get(pos).getBrandName());
                            txtVehicleModels.setText(ourVehicleArrayList.get(pos).getModel());
                            txtVehicleType.setText(ourVehicleArrayList.get(pos).getVehicleType());
                            txtFuelCapacity.setText(ourVehicleArrayList.get(pos).getFuelCap());

                            fusedLocationClient.getLastLocation()
                                    .addOnSuccessListener(this, location -> {
                                        // Got last known location. In some rare situations this can be null.
                                        if (location != null) {
                                            // Logic to handle location object
                                            sourceLatitute = location.getLatitude();
                                            soureLongitute = location.getLongitude();
                                            Log.d(TAG, "onSuccess: " + sourceLatitute + " " + soureLongitute);
                                            txtSourceLocation.setText(MyUtliz.getAddress(MainActivity.this, sourceLatitute, soureLongitute).getAddressLine(0));
                                            if (ourVehicleDialog.isShowing()) {
                                                ourVehicleDialog.dismiss();
                                            }
                                            locationService = new Intent(MainActivity.this, LocationCaptureService.class);
                                            startService(locationService);
                                        }
                                    });

                        }
                    } else {
                        txtStartLocation.setText("Stop Travelling");
                        txtVehicelBrand.setText(ourVehicleArrayList.get(pos).getBrandName());
                        txtVehicleModels.setText(ourVehicleArrayList.get(pos).getModel());
                        txtVehicleType.setText(ourVehicleArrayList.get(pos).getVehicleType());
                        txtFuelCapacity.setText(ourVehicleArrayList.get(pos).getFuelCap());

                        fusedLocationClient.getLastLocation()
                                .addOnSuccessListener(this, location -> {
                                    // Got last known location. In some rare situations this can be null.
                                    if (location != null) {
                                        // Logic to handle location object
                                        sourceLatitute = location.getLatitude();
                                        soureLongitute = location.getLongitude();
                                        Log.d(TAG, "onSuccess: " + sourceLatitute + " " + soureLongitute);
                                        txtSourceLocation.setText(MyUtliz.getAddress(MainActivity.this, sourceLatitute, soureLongitute).getAddressLine(0));
                                        if (ourVehicleDialog.isShowing()) {
                                            ourVehicleDialog.dismiss();
                                        }
                                        locationService = new Intent(MainActivity.this, LocationCaptureService.class);
                                        startService(locationService);
                                    }
                                });

                    }


                }
                break;

            }


        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
        registerReceiver(receiver, new IntentFilter(INTENT_FETCH_STR));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
    }
}
