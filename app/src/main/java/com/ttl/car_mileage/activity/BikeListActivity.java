package com.ttl.car_mileage.activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.ttl.car_mileage.R;
import com.ttl.car_mileage.adapter.BikeListAdapter;
import com.ttl.car_mileage.local_database.DatabaseClient;
import com.ttl.car_mileage.local_database.bike_database.BikeTable;
import com.ttl.car_mileage.local_database.bike_database.BikeTableDao;
import com.ttl.car_mileage.utilz.BaseActivity;
import com.ttl.car_mileage.utilz.CameraGallery;
import com.ttl.car_mileage.utilz.MyUtliz;
import com.ttl.car_mileage.utilz.PathUtilz;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BikeListActivity extends BaseActivity {


    private static final String TAG = "BikeTable";
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.login_btn)
    CardView loginBtn;
    @BindView(R.id.search)
    AppCompatEditText search;
    @BindView(R.id.recy_car_list)
    RecyclerView recyCarList;
    @BindView(R.id.fbtn_add_brand)
    FloatingActionButton fbtnAddBrand;
    private BikeTableDao bikeTableDB;
    private ImageView imgCancel;
    private TextView txtImage;
    private ImageView imgcarBrand;
    private TextView txSubmitDia;
    private EditText edtMileage, edtFuelCapacity, edtCarModel, edtCarBrand;
    private Spinner spnFuelType;

    private ArrayList<String> spnList;

    private File selectedBikeImage;
    private Dialog carBrandDialog;
    private List<BikeTable> bikeTableList;
    private BikeListAdapter bikeListAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bike_list);
        ButterKnife.bind(this);
        title.setText("Bike Brand");
        spnList = new ArrayList<>();
        spnList.add("Petrol");
        spnList.add("Diesel");
        /**
         * initialization DB Object  for Car Brand
         */
        bikeTableDB = DatabaseClient.getInstance(this).getAppDatabase().bikeTableDao();
        bikeTableList = new ArrayList<>();
        recyCarList.setLayoutManager(new GridLayoutManager(this, 3));


        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (bikeTableList !=null && bikeTableList.size()!=0) {
                    ArrayList<BikeTable> filterList =new ArrayList<>();
                    for (BikeTable BikeTable : bikeTableList) {
                        if (BikeTable.getBikeBrand().trim().toLowerCase().contains(s.toString().toLowerCase().trim())) {
                            filterList.add(BikeTable);
                        }
                    }
                    bikeListAdapter.setList(filterList);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

      getAllBrands();
    }

    private void getAllBrands() {
        try {
            MyUtliz.showLoading(this,false);
           class  getcheck extends  AsyncTask<Void,Void,Void>{

               @Override
               protected Void doInBackground(Void... voids) {
                   bikeTableList= bikeTableDB.getBrandName();
                   return null;
               }

               @Override
               protected void onPostExecute(Void aVoid) {
                   super.onPostExecute(aVoid);
                   if (bikeTableList!=null && bikeTableList.size()!=0) {
                       setToadapter();
                   }else {
                       new Handler().postDelayed(()->{
                           saveCar();
                       },1000);
                   }
               }
           }
           new getcheck().execute();
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    @OnClick({R.id.back, R.id.fbtn_add_brand, R.id.login_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back:
                finish();
                break;
            case R.id.fbtn_add_brand:
                digAddBikeBrand();
                break;
            case R.id.login_btn:
                break;
        }
    }

    private void digAddBikeBrand() {
        try {

            carBrandDialog = new Dialog(this);
            carBrandDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            carBrandDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            carBrandDialog.setContentView(R.layout.dialog_add_show_bike_brand);
            carBrandDialog.show();
            imgCancel = carBrandDialog.findViewById(R.id.img_cancel);
            txtImage = carBrandDialog.findViewById(R.id.txt_image);
            imgcarBrand = carBrandDialog.findViewById(R.id.img_car_brand);
            txSubmitDia = carBrandDialog.findViewById(R.id.txt_submit_dia);
            edtMileage = carBrandDialog.findViewById(R.id.edt_mileage);
            spnFuelType = carBrandDialog.findViewById(R.id.spn_fuel_type);
            edtFuelCapacity = carBrandDialog.findViewById(R.id.edt_fuel_capacity);
            edtCarModel = carBrandDialog.findViewById(R.id.edt_car_model);
            edtCarBrand = carBrandDialog.findViewById(R.id.edt_car_brand);

            spnFuelType.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, spnList));

            imgCancel.setOnClickListener(m -> {
                carBrandDialog.dismiss();
            });

            txSubmitDia.setOnClickListener(v -> {

                String carBrand = edtCarBrand.getText().toString();
                String carModel = edtCarModel.getText().toString();
                String fuelCap = edtFuelCapacity.getText().toString();
                String mileage = edtMileage.getText().toString();
                String fuelType = spnFuelType.getSelectedItem().toString();

                if (validation(carBrand, carModel, fuelCap, mileage)) {
                    if (selectedBikeImage != null) {
                        saveLocalBikeTable(carBrand, carModel, fuelCap, mileage, fuelType);
                    } else {
                        MyUtliz.ShowToast(this, "Selected Image");
                    }
                }

            });
            txtImage.setOnClickListener(v -> {
                CameraGallery.showSelectCameraGellery(this, false);
            });


        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private boolean validation(String carBrand, String carModel, String fuelCap, String mileage) {
        if (carBrand.length() == 0) {
            MyUtliz.ShowToast(this, "Bike Brand Can't Be Empty");
            return false;
        } else if (carModel.length() == 0) {
            MyUtliz.ShowToast(this, "Bike Model Can't Be Empty");
            return false;
        } else if (fuelCap.length() == 0) {
            MyUtliz.ShowToast(this, "Fuel Capacity  Can't Be Empty");
            return false;
        } else if (mileage.length() == 0) {
            MyUtliz.ShowToast(this, "Mileage  Can't Be Empty");
            return false;
        } else {
            return true;
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CameraGallery.REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK && data != null) {
            if (data.getData() != null) {
                Bundle extras = data.getExtras();
                Bitmap imageBitmap = (Bitmap) extras.get("data");
                File file = MyUtliz.fileFolderCreated("ApplicationLocalData", Calendar.getInstance().getTimeInMillis() + ".png");
                selectedBikeImage = MyUtliz.conBitmapToFile(imageBitmap, file);
                Log.d(TAG, "onActivityResult: " + selectedBikeImage.getName());

            }
        } else if (requestCode == CameraGallery.SELECT_IMAGE && resultCode == Activity.RESULT_OK && data != null) {
            selectedBikeImage = PathUtilz.getPathFromUri(this, data.getData());
            Log.d(TAG, "onActivityResult: " + selectedBikeImage.getName());
        }
        if (selectedBikeImage != null) {
            imgcarBrand.setVisibility(View.VISIBLE);
            MyUtliz.loadfiletoIMG(selectedBikeImage, imgcarBrand);
        }
    }

    private void saveLocalBikeTable(String bikeBrand, String bikeModel, String fuelCap, String mileage, String fuelType) {
        try {
            BikeTable BikeTable = new BikeTable(
                    bikeBrand,
                    bikeModel,
                    mileage,
                    fuelCap, fuelType, MyUtliz.convertFileToBytes(selectedBikeImage)
            );

            class AddBikeBranData extends AsyncTask<Void, Void, Void> {
                BikeTable data;
                boolean isSucesss = false;

                @Override
                protected void onPreExecute() {
                    super.onPreExecute();

                }

                @Override
                protected Void doInBackground(Void... voids) {
                    data = bikeTableDB.checkCarBrand(bikeBrand, bikeModel);
                    if (data != null) {
                        isSucesss = false;
                    } else {
                        isSucesss = true;
                        bikeTableDB.insert(BikeTable);
                        bikeTableList.clear();
                        bikeTableList = bikeTableDB.getBrandName();
                    }

                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);

                    if (isSucesss) {
                        if (carBrandDialog != null && carBrandDialog.isShowing()) {
                            carBrandDialog.dismiss();
                        }
                        selectedBikeImage = null;
                        bikeListAdapter =  new BikeListAdapter(BikeListActivity.this, bikeTableList);
                        recyCarList.setAdapter(bikeListAdapter);
                        MyUtliz.ShowToast(BikeListActivity.this, "Car Brand Added SuccessFully");
                    } else {
                        MyUtliz.ShowToast(BikeListActivity.this, "Invalid Data Brand and Model");
                    }
                }
            }
            new AddBikeBranData().execute();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void saveCar() {
        try {

            bikeTableList.add(new BikeTable("Ducati", "Scrambler 1100 Sport PRO", "50", "10", "Petrol", MyUtliz.convetDrawble(getDrawable(R.drawable.ducati))));
            bikeTableList.add(new BikeTable("Ducati", "Panigale Superleggera V4", "50", "10", "Petrol", MyUtliz.convetDrawble(getDrawable(R.drawable.ducati))));
            bikeTableList.add(new BikeTable("Ducati", "Hypermotard 950 / 950 SP", "50", "10", "Petrol", MyUtliz.convetDrawble(getDrawable(R.drawable.ducati))));
            bikeTableList.add(new BikeTable("Ducati", "Monster 821 Stealth", "50", "10", "Petrol", MyUtliz.convetDrawble(getDrawable(R.drawable.ducati))));
            bikeTableList.add(new BikeTable("Ducati", "Monster 821", "50", "10", "Petrol", MyUtliz.convetDrawble(getDrawable(R.drawable.ducati))));


            bikeTableList.add(new BikeTable("Harley Davidson", "Harley Davidson Fat Boy", "50", "10", "Petrol", MyUtliz.convetDrawble(getDrawable(R.drawable.hardely_devision))));
            bikeTableList.add(new BikeTable("Harley Davidson", "Harley Davidson  Street Rod", "50", "10", "Petrol", MyUtliz.convetDrawble(getDrawable(R.drawable.hardely_devision))));
            bikeTableList.add(new BikeTable("Harley Davidson", "Harley Davidson 1200 Custom", "50", "10", "Petrol", MyUtliz.convetDrawble(getDrawable(R.drawable.hardely_devision))));
            bikeTableList.add(new BikeTable("Harley Davidson", "Harley Davidson Low Rider", "50", "10", "Petrol", MyUtliz.convetDrawble(getDrawable(R.drawable.hardely_devision))));


            bikeTableList.add(new BikeTable("Royal Enfield", "Classic 350 ", "50", "10", "Petrol", MyUtliz.convetDrawble(getDrawable(R.drawable.royal_enfi))));
            bikeTableList.add(new BikeTable("Royal Enfield", "interceptor 650 ", "50", "10", "Petrol", MyUtliz.convetDrawble(getDrawable(R.drawable.royal_enfi))));
            bikeTableList.add(new BikeTable("Royal Enfield", "Bullet 350", "50", "10", "Petrol", MyUtliz.convetDrawble(getDrawable(R.drawable.royal_enfi))));
            bikeTableList.add(new BikeTable("Royal Enfield", "Classic 500", "50", "10", "Petrol", MyUtliz.convetDrawble(getDrawable(R.drawable.royal_enfi))));
            bikeTableList.add(new BikeTable("Royal Enfield", "ThunderBird 350", "50", "10", "Petrol", MyUtliz.convetDrawble(getDrawable(R.drawable.royal_enfi))));


            bikeTableList.add(new BikeTable("YAMAHA", "MT5", "50", "10", "Petrol", MyUtliz.convetDrawble(getDrawable(R.drawable.yamaha))));
            bikeTableList.add(new BikeTable("YAMAHA", "FZ", "50", "10", "Petrol", MyUtliz.convetDrawble(getDrawable(R.drawable.yamaha))));
            bikeTableList.add(new BikeTable("YAMAHA", "R15", "50", "10", "Petrol", MyUtliz.convetDrawble(getDrawable(R.drawable.yamaha))));


            bikeTableList.add(new BikeTable("Hero", "Splendor Pro", "50", "10", "Petrol", MyUtliz.convetDrawble(getDrawable(R.drawable.hero))));
            bikeTableList.add(new BikeTable("Hero", "Passion Pro", "50", "10", "Petrol", MyUtliz.convetDrawble(getDrawable(R.drawable.hero))));
            bikeTableList.add(new BikeTable("Hero", " HF Deluxe ", "50", "10", "Petrol", MyUtliz.convetDrawble(getDrawable(R.drawable.hero))));

            bikeTableList.add(new BikeTable("TVS", "Apache RTR 180 ", "50", "10", "Petrol", MyUtliz.convetDrawble(getDrawable(R.drawable.tvs))));
            bikeTableList.add(new BikeTable("TVS", "Apache RR 310", "50", "10", "Petrol", MyUtliz.convetDrawble(getDrawable(R.drawable.tvs))));
            bikeTableList.add(new BikeTable("TVS", "NTORQ 125", "50", "10", "Petrol", MyUtliz.convetDrawble(getDrawable(R.drawable.tvs))));

            bikeTableList.add(new BikeTable("KTM", "KTM DUKE 200", "50", "10", "Petrol", MyUtliz.convetDrawble(getDrawable(R.drawable.ktm))));
            bikeTableList.add(new BikeTable("KTM", "KTM DUKE 390", "50", "10", "Petrol", MyUtliz.convetDrawble(getDrawable(R.drawable.ktm))));
            bikeTableList.add(new BikeTable("KTM", "KTM RC 200", "50", "10", "Petrol", MyUtliz.convetDrawble(getDrawable(R.drawable.ktm))));

            bikeTableList.add(new BikeTable("Honda", "Shine", "50", "10", "Petrol", MyUtliz.convetDrawble(getDrawable(R.drawable.honda))));


            class defaultBikeList extends AsyncTask<Void, Void, Void> {



                @Override
                protected void onPreExecute() {

                    super.onPreExecute();
                }

                @Override
                protected Void doInBackground(Void... voids) {
                    if (bikeTableDB.getAll().size() != 0) {
                        bikeTableDB.truncateCarTable();
                    }
                    bikeTableDB.insertAll(bikeTableList);
                    bikeTableList = bikeTableDB.getBrandName();
                    Log.d(TAG, "doInBackground: " + bikeTableList.size());
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                        setToadapter();
                }
            }

            new defaultBikeList().execute();

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    private void setToadapter() {
        try {
            MyUtliz.loadingDismiss();
            bikeListAdapter =  new BikeListAdapter(BikeListActivity.this, bikeTableList);
            recyCarList.setAdapter(bikeListAdapter);
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
}

