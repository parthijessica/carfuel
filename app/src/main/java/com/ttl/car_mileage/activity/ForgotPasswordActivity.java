package com.ttl.car_mileage.activity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import androidx.appcompat.widget.AppCompatEditText;

import com.ttl.car_mileage.R;
import com.ttl.car_mileage.utilz.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ForgotPasswordActivity extends BaseActivity {


    @BindView(R.id.email_txt)
    AppCompatEditText emailTxt;
    @BindView(R.id.login_btn)
    Button loginBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        ButterKnife.bind(this);

    }

    @OnClick(R.id.login_btn)
    public void onViewClicked() {
        startActivity(new Intent(getContext(), SetPasswordActivity.class));
    }
}
