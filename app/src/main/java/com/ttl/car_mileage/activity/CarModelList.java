package com.ttl.car_mileage.activity;

import android.app.Dialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.ttl.car_mileage.R;
import com.ttl.car_mileage.adapter.CarListAdapter;
import com.ttl.car_mileage.adapter.CarModelsListAdapter;
import com.ttl.car_mileage.local_database.DatabaseClient;
import com.ttl.car_mileage.local_database.car_table.CarTable;
import com.ttl.car_mileage.local_database.car_table.CarTableDao;
import com.ttl.car_mileage.utilz.BaseActivity;
import com.ttl.car_mileage.utilz.CameraGallery;
import com.ttl.car_mileage.utilz.MyUtliz;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class CarModelList extends BaseActivity {


    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.login_btn)
    CardView loginBtn;
    @BindView(R.id.search)
    AppCompatEditText search;
    @BindView(R.id.recy_car_list)
    RecyclerView recyCarList;
    @BindView(R.id.fbtn_add_brand)
    FloatingActionButton fbtnAddBrand;
    private String carBrand;
    private CarTableDao carTableDB;
    private List<CarTable> carModelList;
    private CarModelsListAdapter carModelsListAdapter;

    private ImageView imgCancel;
    private TextView txtImage;
    private ImageView imgcarBrand;
    private TextView txSubmitDia;
    private EditText edtMileage, edtFuelCapacity, edtCarModel, edtCarBrand;
    private Dialog carBrandDialog;
    private Spinner spnFuelType;
    private File selectedCarImage;

    private ArrayList<String> spnList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_models);
        ButterKnife.bind(this);

        if (getIntent().hasExtra("brand")) {
            carBrand = getIntent().getStringExtra("brand");
            title.setText(carBrand);
        }
        spnList = new ArrayList<>();
        spnList.add("Petrol");
        spnList.add("Diesel");

        carTableDB = DatabaseClient.getInstance(this).getAppDatabase().carTableDao();
        getAllModel();
        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (carModelList!=null && carModelList.size()!=0) {

                    ArrayList<CarTable>  filterList = new ArrayList<>();

                    for (int i = 0; i < carModelList.size(); i++) {

                        if (carModelList.get(i).getCarModel().trim().toLowerCase().contains(s.toString().toLowerCase().trim())) {
                            filterList.add(carModelList.get(i));
                        }
                    }
                    carModelsListAdapter.setList(filterList);


                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void getAllModel() {
        try {
            class getCarModel extends AsyncTask<Void,Void,Void>{
                @Override
                protected Void doInBackground(Void... voids) {
                    carModelList =carTableDB.getModels(carBrand);
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    setAdapter();
                }
            }
            new getCarModel().execute();
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    private void setAdapter() {
        try {
            recyCarList.setLayoutManager(new LinearLayoutManager(this));
            carModelsListAdapter=  new CarModelsListAdapter(this,carModelList);
            recyCarList.setAdapter(carModelsListAdapter);
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    @OnClick({R.id.back, R.id.search, R.id.fbtn_add_brand})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back:
                finish();
                break;
            case R.id.search:
                break;
            case R.id.fbtn_add_brand:
                digAddcarModel();
                break;
        }
    }

    private void digAddcarModel() {
        try {

            carBrandDialog = new Dialog(this);
            carBrandDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            carBrandDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            carBrandDialog.setContentView(R.layout.dialog_add_show_car_model);
            carBrandDialog.show();
            imgCancel = carBrandDialog.findViewById(R.id.img_cancel);
            txtImage = carBrandDialog.findViewById(R.id.txt_image);
            txSubmitDia = carBrandDialog.findViewById(R.id.txt_submit_dia);
            edtMileage = carBrandDialog.findViewById(R.id.edt_mileage);
            spnFuelType = carBrandDialog.findViewById(R.id.spn_fuel_type);
            edtFuelCapacity = carBrandDialog.findViewById(R.id.edt_fuel_capacity);
            edtCarModel = carBrandDialog.findViewById(R.id.edt_car_model);

            spnFuelType.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, spnList));

            imgCancel.setOnClickListener(m -> {
                carBrandDialog.dismiss();
            });

            txSubmitDia.setOnClickListener(v -> {

                String carModel = edtCarModel.getText().toString();
                String fuelCap = edtFuelCapacity.getText().toString();
                String mileage = edtMileage.getText().toString();
                String fuelType = spnFuelType.getSelectedItem().toString();

                if (validation(carModel, fuelCap, mileage)) {

                        saveLocalCarTable(carModel, fuelCap, mileage, fuelType);

                }

            });



        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private boolean validation( String carModel, String fuelCap, String mileage) {
      if (carModel.length() == 0) {
            MyUtliz.ShowToast(this, "Car Model Can't Be Empty");
            return false;
        } else if (fuelCap.length() == 0) {
            MyUtliz.ShowToast(this, "Fuel Capacity  Can't Be Empty");
            return false;
        } else if (mileage.length() == 0) {
            MyUtliz.ShowToast(this, "Mileage  Can't Be Empty");
            return false;
        } else {
            return true;
        }
    }

    private void saveLocalCarTable(String carModel, String fuelCap, String mileage, String fuelType) {
        try {
            CarTable carTable = new CarTable(
                    carBrand,
                    carModel,
                    mileage,
                    fuelCap, fuelType,carModelList.get(0).getImage()
            );

            class AddCarBranData extends AsyncTask<Void, Void, Void> {
                CarTable data;
                boolean isSucesss = false;

                @Override
                protected Void doInBackground(Void... voids) {
                    data = carTableDB.checkCarBrand(carBrand, carModel);
                    if (data != null) {
                        isSucesss = false;
                    } else {
                        isSucesss = true;
                        carTableDB.insert(carTable);
                        carModelList.clear();
                        carModelList =carTableDB.getModels(carBrand);
                    }

                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    if (isSucesss) {
                        if (carBrandDialog != null && carBrandDialog.isShowing()) {
                            carBrandDialog.dismiss();
                        }
                        selectedCarImage = null;
                        carModelsListAdapter=  new CarModelsListAdapter(CarModelList.this,carModelList);
                        recyCarList.setAdapter(carModelsListAdapter);
                        MyUtliz.ShowToast(CarModelList.this, "Car Model Added SuccessFully");
                    } else {
                        MyUtliz.ShowToast(CarModelList.this, "Invalid Data Brand and Model");
                    }
                }
            }
            new AddCarBranData().execute();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
