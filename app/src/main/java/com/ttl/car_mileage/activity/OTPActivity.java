package com.ttl.car_mileage.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;

import com.ttl.car_mileage.R;
import com.ttl.car_mileage.utilz.BaseActivity;
import com.ttl.car_mileage.utilz.CustomOtpEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OTPActivity extends BaseActivity {


    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.login_btn)
    CardView loginBtn;
    @BindView(R.id.mobile_number)
    TextView mobileNumber;
    @BindView(R.id.otp)
    CustomOtpEditText otp;
    @BindView(R.id.register_btn)
    Button registerBtn;
    @BindView(R.id.resent_otp)
    TextView resentOtp;
    @BindView(R.id.call_request)
    Button callRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        ButterKnife.bind(this);
        title.setText("OTP");
    }

    @OnClick({R.id.back, R.id.register_btn, R.id.resent_otp, R.id.call_request})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back:
                onBackPressed();
                break;
            case R.id.register_btn:
                break;
            case R.id.resent_otp:
                otp.setText("");
                break;
            case R.id.call_request:
                break;
        }
    }
}
