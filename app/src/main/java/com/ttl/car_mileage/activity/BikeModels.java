package com.ttl.car_mileage.activity;

import android.app.Dialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.ttl.car_mileage.R;
import com.ttl.car_mileage.adapter.BikeModelAdapter;
import com.ttl.car_mileage.local_database.DatabaseClient;
import com.ttl.car_mileage.local_database.bike_database.BikeTable;
import com.ttl.car_mileage.local_database.bike_database.BikeTableDao;
import com.ttl.car_mileage.utilz.BaseActivity;
import com.ttl.car_mileage.utilz.MyUtliz;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BikeModels extends BaseActivity {



    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.login_btn)
    CardView loginBtn;
    @BindView(R.id.search)
    AppCompatEditText search;
    @BindView(R.id.recy_car_list)
    RecyclerView recyCarList;
    @BindView(R.id.fbtn_add_brand)
    FloatingActionButton fbtnAddBrand;
    private String bikeBrand;
    private BikeTableDao bikeTableDao;
    private List<BikeTable> bikeList;
    private BikeModelAdapter bikeModelAdapter;

    private ImageView imgCancel;
    private TextView txtImage;
    private ImageView imgcarBrand;
    private TextView txSubmitDia;
    private EditText edtMileage, edtFuelCapacity, edtCarModel, edtCarBrand;
    private Dialog carBrandDialog;
    private Spinner spnFuelType;
    private File selectedCarImage;

    private ArrayList<String> spnList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_models);
        ButterKnife.bind(this);

        if (getIntent().hasExtra("brand")) {
            bikeBrand = getIntent().getStringExtra("brand");
            title.setText(bikeBrand);
        }
        spnList = new ArrayList<>();
        spnList.add("Petrol");
        spnList.add("Diesel");

        bikeTableDao = DatabaseClient.getInstance(this).getAppDatabase().bikeTableDao();
        getAllModel();
        search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (bikeList !=null && bikeList.size()!=0) {

                    ArrayList<BikeTable>  filterList = new ArrayList<>();

                    for (int i = 0; i < bikeList.size(); i++) {

                        if (bikeList.get(i).getBikeModel().trim().toLowerCase().contains(s.toString().toLowerCase().trim())) {
                            filterList.add(bikeList.get(i));
                        }
                    }
                    bikeModelAdapter.setList(filterList);


                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    private void getAllModel() {
        try {
            class getBikeModel extends AsyncTask<Void,Void,Void> {
                @Override
                protected Void doInBackground(Void... voids) {
                    bikeList = bikeTableDao.getModels(bikeBrand);
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    setAdapter();
                }
            }
            new getBikeModel().execute();
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    private void setAdapter() {
        try {
            recyCarList.setLayoutManager(new LinearLayoutManager(this));
            bikeModelAdapter =  new BikeModelAdapter(this, bikeList);
            recyCarList.setAdapter(bikeModelAdapter);
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    @OnClick({R.id.back, R.id.search, R.id.fbtn_add_brand})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back:
                finish();
                break;
            case R.id.search:
                break;
            case R.id.fbtn_add_brand:
                digAddcarModel();
                break;
        }
    }

    private void digAddcarModel() {
        try {

            carBrandDialog = new Dialog(this);
            carBrandDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            carBrandDialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            carBrandDialog.setContentView(R.layout.dialog_add_show_bike_model);
            carBrandDialog.show();
            imgCancel = carBrandDialog.findViewById(R.id.img_cancel);
            txtImage = carBrandDialog.findViewById(R.id.txt_image);
            txSubmitDia = carBrandDialog.findViewById(R.id.txt_submit_dia);
            edtMileage = carBrandDialog.findViewById(R.id.edt_mileage);
            spnFuelType = carBrandDialog.findViewById(R.id.spn_fuel_type);
            edtFuelCapacity = carBrandDialog.findViewById(R.id.edt_fuel_capacity);
            edtCarModel = carBrandDialog.findViewById(R.id.edt_car_model);

            spnFuelType.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, spnList));

            imgCancel.setOnClickListener(m -> {
                carBrandDialog.dismiss();
            });

            txSubmitDia.setOnClickListener(v -> {

                String bikeModel = edtCarModel.getText().toString();
                String fuelCap = edtFuelCapacity.getText().toString();
                String mileage = edtMileage.getText().toString();
                String fuelType = spnFuelType.getSelectedItem().toString();

                if (validation(bikeModel, fuelCap, mileage)) {

                    saveLocalCarTable(bikeModel, fuelCap, mileage, fuelType);

                }

            });



        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private boolean validation( String carModel, String fuelCap, String mileage) {
        if (carModel.length() == 0) {
            MyUtliz.ShowToast(this, "Bike  Model Can't Be Empty");
            return false;
        } else if (fuelCap.length() == 0) {
            MyUtliz.ShowToast(this, "Fuel Capacity  Can't Be Empty");
            return false;
        } else if (mileage.length() == 0) {
            MyUtliz.ShowToast(this, "Mileage  Can't Be Empty");
            return false;
        } else {
            return true;
        }
    }

    private void saveLocalCarTable(String bikeModel, String fuelCap, String mileage, String fuelType) {
        try {
            BikeTable bikeTable = new BikeTable(
                    bikeBrand,
                    bikeModel,
                    mileage,
                    fuelCap, fuelType, bikeList.get(0).getImage()
            );

            class AddBikeBranData extends AsyncTask<Void, Void, Void> {
                BikeTable data;
                boolean isSucesss = false;

                @Override
                protected Void doInBackground(Void... voids) {
                    data = bikeTableDao.checkCarBrand(bikeBrand, bikeModel);
                    if (data != null) {
                        isSucesss = false;
                    } else {
                        isSucesss = true;
                        bikeTableDao.insert(bikeTable);
                        bikeList.clear();
                        bikeList = bikeTableDao.getModels(bikeBrand);
                    }

                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    if (isSucesss) {
                        if (carBrandDialog != null && carBrandDialog.isShowing()) {
                            carBrandDialog.dismiss();
                        }
                        selectedCarImage = null;
                        bikeModelAdapter =  new BikeModelAdapter(BikeModels.this, bikeList);
                        recyCarList.setAdapter(bikeModelAdapter);
                        MyUtliz.ShowToast(BikeModels.this, "Bike  Model Added SuccessFully");
                    } else {
                        MyUtliz.ShowToast(BikeModels.this, "Invalid Data Brand and Model");
                    }
                }
            }
            new AddBikeBranData().execute();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
