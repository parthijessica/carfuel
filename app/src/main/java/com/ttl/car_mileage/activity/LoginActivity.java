package com.ttl.car_mileage.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;

import com.ttl.car_mileage.R;
import com.ttl.car_mileage.local_database.DatabaseClient;
import com.ttl.car_mileage.local_database.user.UserTable;
import com.ttl.car_mileage.local_database.user.UserTableDao;
import com.ttl.car_mileage.utilz.BaseActivity;
import com.ttl.car_mileage.utilz.LocalSession;
import com.ttl.car_mileage.utilz.MyUtliz;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends BaseActivity {


    private static final String TAG = "Login";
    @BindView(R.id.email_txt)
    AppCompatEditText emailTxt;
    @BindView(R.id.password_txt)
    AppCompatEditText passwordTxt;

    @BindView(R.id.forgot_password)
    TextView forgotPassword;
    @BindView(R.id.sign_up)
    LinearLayout signUp;
    @BindView(R.id.login_btn)
    AppCompatTextView loginBtn;
    private UserTableDao userDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        /**
         * intialzing UserTable Dao
         */
        userDB = DatabaseClient.getInstance(this).getAppDatabase().userTableDao();
    }

    @OnClick({R.id.sign_up, R.id.login_btn, R.id.forgot_password})
    public void onViewClicked(View view) {
        switch (view.getId()) {


            case R.id.forgot_password:
                startActivity(new Intent(getContext(), ForgotPasswordActivity.class));
                break;
            case R.id.login_btn:
                String username = emailTxt.getText().toString();
                String password = passwordTxt.getText().toString();
                if (validation(username, password)) {
                    checkLocalDB(username, password);
                }
                break;
            case R.id.sign_up:
                startActivity(new Intent(getContext(), SignUpActivity.class));
                break;
        }
    }

    private void checkLocalDB(String username, String password) {
        try {
            class CheckLogin extends AsyncTask<Void, Void, Void> {
                UserTable data = null;

                @Override
                protected Void doInBackground(Void... voids) {
                    data = userDB.userLogin(password, username);
                    return null;
                }

                @Override
                protected void onPostExecute(Void aVoid) {
                    super.onPostExecute(aVoid);
                    if (data != null) {
                        LocalSession.setString(LoginActivity.this, LocalSession.EMAIL, data.getUserEmail());
                        LocalSession.setString(LoginActivity.this, LocalSession.USER_ID, String.valueOf(data.getUser_id()));
                        LocalSession.setString(LoginActivity.this, LocalSession.NAME, data.getUserName());
                        LocalSession.setString(LoginActivity.this, LocalSession.PHONE, data.getUserPhone());
                        LocalSession.setBoolean(LoginActivity.this, LocalSession.isLogged, true);
                        MyUtliz.ShowToast(LoginActivity.this, "Login  Successfully");
                        startActivity(new Intent(LoginActivity.this, MainActivity.class));
                    } else {
                        MyUtliz.ShowToast(LoginActivity.this, "Invalid Username Password");
                    }
                }


            }
            CheckLogin check = new CheckLogin();
            check.execute();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private boolean validation(String username, String password) {
        if (username.length() == 0) {
            MyUtliz.ShowToast(this, "User Name Can't Empty");
            return false;
        } else if (password.length() == 0) {
            MyUtliz.ShowToast(this, "Password");
            return false;
        } else {
            return true;
        }
    }


}
