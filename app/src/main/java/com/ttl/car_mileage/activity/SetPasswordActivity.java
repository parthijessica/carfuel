package com.ttl.car_mileage.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.widget.AppCompatEditText;
import androidx.cardview.widget.CardView;

import com.ttl.car_mileage.R;
import com.ttl.car_mileage.utilz.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SetPasswordActivity extends BaseActivity {


    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.title)
    TextView title;

    @BindView(R.id.password_txt)
    AppCompatEditText passwordTxt;
    @BindView(R.id.visible)
    ImageView visible;
    @BindView(R.id.conform_password_txt)
    AppCompatEditText conformPasswordTxt;
    @BindView(R.id.conform_visible)
    ImageView conformVisible;
    @BindView(R.id.login_btn)
    Button loginBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_password);
        ButterKnife.bind(this);
        title.setText("Set Password");
    }

    @OnClick({R.id.back, R.id.login_btn, R.id.visible, R.id.conform_visible})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back:
                onBackPressed();
                break;
            case R.id.login_btn:
                break;
            case R.id.visible:
                break;
            case R.id.conform_visible:
                break;
        }
    }
}
