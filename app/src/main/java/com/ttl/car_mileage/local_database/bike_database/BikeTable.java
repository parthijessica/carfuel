package com.ttl.car_mileage.local_database.bike_database;


import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity(tableName = "bike_table",indices = {@Index(value = {"bike_model"},
        unique = true)})
public class BikeTable {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "id")
    private int bike_br_id =0;

    @NonNull
    @ColumnInfo(name = "bike_brand")
    private String bikeBrand;

    @NonNull
    @ColumnInfo(name = "bike_model")
    private String bikeModel;

    @NonNull
    @ColumnInfo(name = "mileage")
    private String milaege;

    @NonNull
    @ColumnInfo(name = "fuel_cap")
    private String fuelCap;

    @NonNull
    @ColumnInfo(name = "fuel_type")
    private String fuelType;

    public int getBike_br_id() {
        return bike_br_id;
    }

    public void setBike_br_id(int bike_br_id) {
        this.bike_br_id = bike_br_id;
    }

    @NonNull
    public String getBikeBrand() {
        return bikeBrand;
    }

    public void setBikeBrand(@NonNull String bikeBrand) {
        this.bikeBrand = bikeBrand;
    }

    @NonNull
    public String getBikeModel() {
        return bikeModel;
    }

    public void setBikeModel(@NonNull String bikeModel) {
        this.bikeModel = bikeModel;
    }

    @NonNull
    public String getFuelType() {
        return fuelType;
    }

    public void setFuelType(@NonNull String fuelType) {
        this.fuelType = fuelType;
    }

    @ColumnInfo(typeAffinity = ColumnInfo.BLOB,name = "image")
    private byte[] image;

    public BikeTable(@NonNull String bikeBrand, @NonNull String bikeModel, @NonNull String milaege, @NonNull String fuelCap, @NonNull String fuelType, byte[] image) {
        this.bikeBrand = bikeBrand;
        this.bikeModel = bikeModel;
        this.milaege = milaege;
        this.fuelCap = fuelCap;
        this.fuelType = fuelType;
        this.image = image;
    }


    @NonNull
    public String getMilaege() {
        return milaege;
    }

    public void setMilaege(@NonNull String milaege) {
        this.milaege = milaege;
    }

    @NonNull
    public String getFuelCap() {
        return fuelCap;
    }

    public void setFuelCap(@NonNull String fuelCap) {
        this.fuelCap = fuelCap;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }
}
