package com.ttl.car_mileage.local_database.common;

import android.graphics.Bitmap;

public class ShowOurVehicle {
    Bitmap image;
    String brandName;
    String model;
    int id;
    String vehicleType;
    String fuelCap;
    String mileage;

    public String getMileage() {
        return mileage;
    }

    public void setMileage(String mileage) {
        this.mileage = mileage;
    }

    public String getFuelCap() {
        return fuelCap;
    }

    public void setFuelCap(String fuelCap) {
        this.fuelCap = fuelCap;
    }

    public ShowOurVehicle(Bitmap image, String brandName, String model, int id, String vehicleType, String fuelCap, String mileage) {
        this.image = image;
        this.brandName = brandName;
        this.model = model;
        this.id = id;
        this.vehicleType = vehicleType;
        this.fuelCap = fuelCap;
        this.mileage = mileage;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }



    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }
}
