package com.ttl.car_mileage.local_database.fuel_entry;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;


@Dao
public interface FuelFillsTableDao {

    @Query("SELECT * FROM fuel_fills_table")
    List<FuelEntry> getAll();

    @Query("SELECT * FROM fuel_fills_table WHERE ref_id =:refID ")
    FuelEntry getFuelFills(String refID);

    @Insert
    void insert(FuelEntry fuelEntry);



}