package com.ttl.car_mileage.local_database.fuel_entry;


import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity(tableName = "fuel_fills_table")
public class FuelEntry {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "id")
    private int fuelFillID =0;

    @NonNull
    @ColumnInfo(name = "ref_id")
    private int refID;

    @NonNull
    @ColumnInfo(name = "vehicle_type")
    private String vehicleType;

    @NonNull
    @ColumnInfo(name = "date_time")
    private long dateTime;
    @NonNull
    @ColumnInfo(name = "fills_value")
    private String fillsValue;

    @NonNull
    @ColumnInfo(name = "driving_speed")
    private String drvingSpeed;

    @NonNull
    public String getDrvingSpeed() {
        return drvingSpeed;
    }

    public void setDrvingSpeed(@NonNull String drvingSpeed) {
        this.drvingSpeed = drvingSpeed;
    }

    public int getFuelFillID() {
        return fuelFillID;
    }

    public void setFuelFillID(int fuelFillID) {
        this.fuelFillID = fuelFillID;
    }

    public int getRefID() {
        return refID;
    }

    public void setRefID(int refID) {
        this.refID = refID;
    }

    @NonNull
    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(@NonNull String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public long getDateTime() {
        return dateTime;
    }

    public void setDateTime(long dateTime) {
        this.dateTime = dateTime;
    }

    @NonNull
    public String getFillsValue() {
        return fillsValue;
    }

    public void setFillsValue(@NonNull String fillsValue) {
        this.fillsValue = fillsValue;
    }

    public FuelEntry(int refID, @NonNull String vehicleType, long dateTime, @NonNull String fillsValue, @NonNull String drvingSpeed) {
        this.refID = refID;
        this.vehicleType = vehicleType;
        this.dateTime = dateTime;
        this.fillsValue = fillsValue;
        this.drvingSpeed = drvingSpeed;
    }
}
