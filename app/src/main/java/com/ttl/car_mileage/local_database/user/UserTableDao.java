package com.ttl.car_mileage.local_database.user;


import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface UserTableDao {

    @Query("SELECT * FROM user_table")
    List<UserTable> getAll();

    @Query("SELECT * FROM user_table WHERE user_phone =:phone or user_email=:email")
    UserTable verifycationEmailPhone(String phone, String email);

    @Query("SELECT * FROM user_table WHERE user_password =:password and user_email=:email")
    UserTable userLogin(String password, String email);

    @Insert
    void insert(UserTable userTable);

    @Delete
    void delete(UserTable user);

}