package com.ttl.car_mileage.local_database.user;


import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity(tableName = "user_table",indices = {@Index(value = {"user_email", "user_phone"},
        unique = true)})
public class UserTable {
    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "id")
    private int user_id=0;

    @NonNull
    @ColumnInfo(name = "user_name")
    private String userName;

    @NonNull
    @ColumnInfo(name = "user_email")
    private String userEmail;

    @NonNull
    @ColumnInfo(name = "user_phone")
    private String userPhone;

    @NonNull
    @ColumnInfo(name = "user_password")
    private String userPassword;

    public UserTable(@NonNull String userName, @NonNull String userEmail, @NonNull String userPhone, @NonNull String userPassword) {
        this.userName = userName;
        this.userEmail = userEmail;
        this.userPhone = userPhone;
        this.userPassword = userPassword;
    }

    @NonNull
    public String getUserName() {
        return userName;
    }

    public void setUserName(@NonNull String userName) {
        this.userName = userName;
    }

    @NonNull
    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(@NonNull String userEmail) {
        this.userEmail = userEmail;
    }

    @NonNull
    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(@NonNull String userPhone) {
        this.userPhone = userPhone;
    }

    @NonNull
    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(@NonNull String userPassword) {
        this.userPassword = userPassword;
    }
}