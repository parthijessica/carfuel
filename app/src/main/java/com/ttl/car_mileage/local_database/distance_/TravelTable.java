package com.ttl.car_mileage.local_database.distance_;


import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "travel_table")
public class TravelTable {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "id")
    private int travel_id = 0;

    @NonNull
    @ColumnInfo(name = "travel_distance")
    private String travel_distance;

    @NonNull
    @ColumnInfo(name = "travel_speed")
    private String travelSpeed;


    @NonNull
    @ColumnInfo(name = "source")
    private String source;

    @NonNull
    @ColumnInfo(name = "designation")
    private String designation;

    @NonNull
    @ColumnInfo(name = "vehicle_type")
    private String vehicleType;

    @NonNull
    @ColumnInfo(name = "vehicle_brand")
    private String vehicleBrand;

    @NonNull
    @ColumnInfo(name = "vehicle_model")
    private String vehicleModel;

    @NonNull
    @ColumnInfo(name = "created_at")
    private long createdAt;

    public long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }

    @NonNull
    @ColumnInfo(name = "fuel_cap")
    private String fuelCap;

    @NonNull
    public String getSource() {
        return source;
    }

    public void setSource(@NonNull String source) {
        this.source = source;
    }

    @NonNull
    public String getVehicleBrand() {
        return vehicleBrand;
    }

    public void setVehicleBrand(@NonNull String vehicleBrand) {
        this.vehicleBrand = vehicleBrand;
    }

    @NonNull
    public String getVehicleModel() {
        return vehicleModel;
    }

    public void setVehicleModel(@NonNull String vehicleModel) {
        this.vehicleModel = vehicleModel;
    }

    @NonNull
    public String getFuelCap() {
        return fuelCap;
    }

    public void setFuelCap(@NonNull String fuelCap) {
        this.fuelCap = fuelCap;
    }

    @NonNull
    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(@NonNull String vehicleType) {
        this.vehicleType = vehicleType;
    }

    @NonNull
    public String getDesignation() {
        return designation;
    }

    public void setDesignation(@NonNull String designation) {
        this.designation = designation;
    }

    @NonNull
    @ColumnInfo(name = "refer_id")
    private int refer_id;

    public int getTravel_id() {
        return travel_id;
    }

    public void setTravel_id(int travel_id) {
        this.travel_id = travel_id;
    }

    @NonNull
    public String getTravel_distance() {
        return travel_distance;
    }

    public void setTravel_distance(@NonNull String travel_distance) {
        this.travel_distance = travel_distance;
    }

    @NonNull
    public String getTravelSpeed() {
        return travelSpeed;
    }

    public void setTravelSpeed(@NonNull String travelSpeed) {
        this.travelSpeed = travelSpeed;
    }

    @NonNull
    public int getRefer_id() {
        return refer_id;
    }

    public void setRefer_id(@NonNull int refer_id) {
        this.refer_id = refer_id;
    }

    public TravelTable(@NonNull String travel_distance, @NonNull String travelSpeed, @NonNull String source, @NonNull String designation, @NonNull String vehicleType, @NonNull String vehicleBrand, @NonNull String vehicleModel, @NonNull String fuelCap, @NonNull int refer_id, @NonNull long createdAt) {
        this.travel_distance = travel_distance;
        this.travelSpeed = travelSpeed;
        this.source = source;
        this.designation = designation;
        this.vehicleType = vehicleType;
        this.vehicleBrand = vehicleBrand;
        this.vehicleModel = vehicleModel;
        this.fuelCap = fuelCap;
        this.refer_id = refer_id;
        this.createdAt = createdAt;
    }
}
