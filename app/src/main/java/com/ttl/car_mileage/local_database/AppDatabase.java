package com.ttl.car_mileage.local_database;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.ttl.car_mileage.local_database.bike_database.BikeTable;
import com.ttl.car_mileage.local_database.bike_database.BikeTableDao;
import com.ttl.car_mileage.local_database.car_table.CarTable;
import com.ttl.car_mileage.local_database.car_table.CarTableDao;
import com.ttl.car_mileage.local_database.distance_.TravelTable;
import com.ttl.car_mileage.local_database.distance_.TravelTableDao;
import com.ttl.car_mileage.local_database.fuel_entry.FuelEntry;
import com.ttl.car_mileage.local_database.fuel_entry.FuelFillsTableDao;
import com.ttl.car_mileage.local_database.user.UserTable;
import com.ttl.car_mileage.local_database.user.UserTableDao;

@Database(entities = {UserTable.class, CarTable.class, BikeTable.class, FuelEntry.class, TravelTable.class}, version = 1)
public  abstract class AppDatabase extends RoomDatabase {
    public abstract UserTableDao userTableDao();
    public abstract CarTableDao  carTableDao();
    public abstract BikeTableDao bikeTableDao();
    public abstract FuelFillsTableDao fuelFillsTableDao();
    public abstract TravelTableDao  travelTableDao();
}
