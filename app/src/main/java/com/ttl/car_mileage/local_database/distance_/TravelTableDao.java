package com.ttl.car_mileage.local_database.distance_;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;


@Dao
public interface TravelTableDao {

    @Query("SELECT * FROM travel_table")
    List<TravelTable> getAll();

    @Insert
    void insert(TravelTable travelTable);

    @Insert
    void insertAll(List<TravelTable> travelTableList);

    @Delete
    void delete(TravelTable travelTable);

    @Query("DELETE from  travel_table")
    void  truncateCarTable();

    @Query("SELECT * FROM travel_table where id =:id")
    TravelTable getTravelData(int id);
}