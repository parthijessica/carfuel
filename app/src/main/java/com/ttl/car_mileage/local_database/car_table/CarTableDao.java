package com.ttl.car_mileage.local_database.car_table;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.ttl.car_mileage.local_database.user.UserTable;

import java.util.List;


@Dao
public interface CarTableDao {

    @Query("SELECT * FROM car_table")
    List<CarTable> getAll();

    @Query("SELECT * FROM car_table WHERE car_brand =:car_brand and car_model=:car_model")
    CarTable  checkCarBrand (String car_brand, String car_model);

    @Query("SELECT * FROM car_table WHERE car_brand =:car_brand and car_model=:car_model")
    CarTable userLogin(String car_brand, String car_model);

    @Insert
    void insert(CarTable carTable);

    @Insert
    void insertAll(List<CarTable> carTableList);

    @Delete
    void delete(CarTable carTable);

    @Query("DELETE from  car_table")
    void  truncateCarTable();

    @Query("SELECT * FROM car_table Group By car_brand")
    List<CarTable> getBrandName();

    @Query("SELECT * FROM car_table where car_brand =:carBrand")
    List<CarTable> getModels(String carBrand);

    @Query("SELECT * FROM car_table where id =:id")
    CarTable getCarData(int id);
}