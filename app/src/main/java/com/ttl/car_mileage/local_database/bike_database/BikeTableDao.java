package com.ttl.car_mileage.local_database.bike_database;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.ttl.car_mileage.local_database.car_table.CarTable;

import java.util.List;


@Dao
public interface BikeTableDao {

    @Query("SELECT * FROM bike_table")
    List<BikeTable> getAll();

    @Query("SELECT * FROM bike_table WHERE bike_brand =:car_brand and bike_model=:car_model")
    BikeTable checkCarBrand(String car_brand, String car_model);

    @Query("SELECT * FROM bike_table WHERE bike_brand  =:car_brand and bike_brand=:car_model")
    BikeTable userLogin(String car_brand, String car_model);

    @Insert
    void insert(BikeTable bikeTable);

    @Insert
    void insertAll(List<BikeTable> bikeTableList);

    @Delete
    void delete(BikeTable bikeTable);

    @Query("DELETE from  bike_table")
    void  truncateCarTable();

    @Query("SELECT * FROM bike_table Group By bike_brand")
    List<BikeTable> getBrandName();

    @Query("SELECT * FROM bike_table where bike_brand =:bikeBrand")
    List<BikeTable> getModels(String bikeBrand);
    @Query("SELECT * FROM bike_table where id =:bikeEntry")
    BikeTable getBikeDate(int bikeEntry);

    @Query("SELECT * FROM bike_table where id =:id")
    BikeTable getBikeData(int id);
}