package com.ttl.car_mileage.local_database.car_table;


import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity(tableName = "car_table",indices = {@Index(value = {"car_model"},
        unique = true)})
public class CarTable {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "id")
    private int car_br_id =0;

    @NonNull
    @ColumnInfo(name = "car_brand")
    private String carBrand;

    @NonNull
    @ColumnInfo(name = "car_model")
    private String carModel;

    @NonNull
    @ColumnInfo(name = "mileage")
    private String milaege;

    @NonNull
    @ColumnInfo(name = "fuel_cap")
    private String fuelCap;

    @NonNull
    @ColumnInfo(name = "fuel_type")
    private String fuelType;

    @NonNull
    public String getFuelType() {
        return fuelType;
    }

    public void setFuelType(@NonNull String fuelType) {
        this.fuelType = fuelType;
    }

    @ColumnInfo(typeAffinity = ColumnInfo.BLOB,name = "image")
    private byte[] image;

    public CarTable(@NonNull String carBrand, @NonNull String carModel, @NonNull String milaege, @NonNull String fuelCap, @NonNull String fuelType, byte[] image) {
        this.carBrand = carBrand;
        this.carModel = carModel;
        this.milaege = milaege;
        this.fuelCap = fuelCap;
        this.fuelType = fuelType;
        this.image = image;
    }

    public int getCar_br_id() {
        return car_br_id;
    }

    public void setCar_br_id(int car_br_id) {
        this.car_br_id = car_br_id;
    }

    @NonNull
    public String getCarBrand() {
        return carBrand;
    }

    public void setCarBrand(@NonNull String carBrand) {
        this.carBrand = carBrand;
    }

    @NonNull
    public String getCarModel() {
        return carModel;
    }

    public void setCarModel(@NonNull String carModel) {
        this.carModel = carModel;
    }

    @NonNull
    public String getMilaege() {
        return milaege;
    }

    public void setMilaege(@NonNull String milaege) {
        this.milaege = milaege;
    }

    @NonNull
    public String getFuelCap() {
        return fuelCap;
    }

    public void setFuelCap(@NonNull String fuelCap) {
        this.fuelCap = fuelCap;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }
}
